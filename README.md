# Simulation of Resonantly Enhanced Third Harmonic Generation in Molecular Gases

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11261875.svg)](https://doi.org/10.5281/zenodo.11261875)
[![Common Changelog](https://common-changelog.org/badge.svg)](https://common-changelog.org)

Simulation notebook for resonantly enhanced third harmonic generation in molecular gases.

For a description see _Detection of HCl molecules by resonantly enhanced sum-frequency mixing of mid- and near-infrared laser pulses_, Benedikt Moneke, Jan Frederic Kinder, Oskar Ernst, and Thomas Halfmann, Phys. Rev. A 107, 012803 – Published 5 January 2023, DOI [10.1103/PhysRevA.107.012803](https://doi.org/10.1103/physreva.107.012803).


## Installation

- Copy this repository.
- Install python and the required packages listed in `pyproject.toml`

## Preparation

The simulation requires transmission data from the HITRAN database.
It also requires, at least one time per isotope, an expensive calculation regarding transition paths.
Execute the script `calculate_tranisition_paths.py` (see the docstring about usage) to download the transmission data and calculate the paths.

## Usage

Open the desired python script in your favorite jupyter application (VS Code, Spyder...)

- `simulation.py` simulates the nonlinear processes
- `position_finder.ipy` allows to plot (interactively) the simulation results for use during an experiment


## Notes on Physical Properties

### Definitions

The following definitions are used.
Check that they correspond to your definitions used

- There is no sign convention on the wave vector mismatch delta_k. Here we use `delta_k = k_output - sum(k_input)`
- The (THG/SFM) phase matching integral contains the confocal parameter `b`: `(2 / b) * np.exp(-1j * delta_k * z) / (1 + 1j * 2 * (z - z_0) / b) ** 2`

### Units
- The most used unit is the wavenumber: the inverse of the wavelength in 1/cm


## Helpful Links

- https://refractiveindex.info : refractive indexes for most solid state materials, community Python package: PyOptik
- https://hitran.org/docs/definitions-and-units/ : Parameters for the HITRAN database

