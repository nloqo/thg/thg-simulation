# -*- coding: utf-8 -*-
"""SFM and THG simulation

Simulates THG and SFM


Created on Tue Dec  6 15:16:45 2022
@author: kinder
"""

# %% Imports and constants
# Imports and constants

# standard lib
import logging
import pickle
from time import perf_counter

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from simulation_methods import (
    make_grid,
    calculate_occupation_for_molecule,
    Processes,
    calculate_absorption_and_phase_mismatch,
    calc_j3_squared_on_grid,
    calculate_chi3,
    THG_efficiency,
    SFM_power,
    calc_all,
    load_stored_lines_and_paths,
    save_spectrum,
)
from hitran_constants import ureg

log = logging.getLogger(__name__)
if not log.handlers:
    log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)

# %%
"""Simulation"""

"""Parameters"""
# If you execute the simulation for a molecule the first time, execute 
# `calculate_tranisition_paths.py`

from molecules.HCl import Molecule  # noqa:E402

molecule = Molecule()


# %% Load data

"""Load the stored data"""


lines, paths = load_stored_lines_and_paths(molecule=molecule)


# %%
# Setup Spectral Simulation
##################################################################################

"""Doing the real simulation to generate a spectrum"""

# %%%
# Define and generate simulation range
"""
HCl
full range: 2790, 2850, 0.001
full range of main peaks: 2827.7, 2834.4, 0.001
full range of main peaks: 2827, 2835, 0.001
N=2 (H35Cl) resonance at 2832.1749839782715: 2832, 2832.4, 0.001
N=3 (H35Cl) resonance at 2830.35876465 (3533.12 nm)
and N=2 (H37Cl) at 2830.15123367: 2830, 2830.8, 0.001

CO2
full range: 2300, 2380, 0.001
interesting range: 2313, 2319, 0.001
two strongest peaks: 2315, 2317, 0.001

fine steps: 0.001 / cm
coarse steps: 0.025 / cm
"""

"""Parameters: Range and temperature"""
limits = 2827, 2835, 0.001  # lower, upper, stepsize. All in 1/cm

# generate grid
nu_grid_fund = make_grid(
    pd.concat(paths.values()),
    # lower limit, upper limit, stepsize
    *limits,
)


# %%%
# Define temperature and calculate occupation

"""Parameters: Temperature"""
temperature = 273.15 + 25  # Kelvin

# Calculate occupation
calculate_occupation_for_molecule(
    molecule=molecule, temperature=temperature, lines=lines, paths=paths
)


# %%%
# Define more simulation parameters
"""Parameters: Process"""
process = Processes.SFM
ignore_electronic_state = False
sfm_probe_wavenumber = 1e7 / 1064  # inverse cm

"""Parameters: Ambient"""
pressure_self = 30 * ureg.hPa
pressure_air = 0 * ureg.hPa

L = 18 * ureg.cm
confocal_parameter = 6 * ureg.cm
z_0 = L / 2

# Conversion
pressure_self = pressure_self.m_as(ureg.Pa)
pressure_air = pressure_air.m_as(ureg.Pa)
L = L.m_as(ureg.m)
confocal_parameter = confocal_parameter.m_as(ureg.m)
z_0 = z_0.m_as(ureg.m)

if process in (Processes.THG, Processes.THG_PERMUTATED):
    nu_grid_signal = 3 * nu_grid_fund
elif process in (Processes.SFM, Processes.SFM_PERMUTATED):
    nu_grid_signal = 2 * nu_grid_fund + sfm_probe_wavenumber
else:
    raise ValueError("Unknown process")


# %%
# Simulation


# %%%
# Calculate linear susceptibility

absorption_coefficient_fund, absorption_coefficient_signal, delta_k = (
    calculate_absorption_and_phase_mismatch(
        nu_grid_fund=nu_grid_fund,
        lines=lines,
        molecule=molecule,
        pressure_self=pressure_self,
        pressure_air=pressure_air,
        process=process,
        sfm_probe_wavenumber=sfm_probe_wavenumber,
    )
)


# %%%
# Calculate phase matching integral j3 (absolute value)

"Slow"
start = perf_counter()
j3_squared = calc_j3_squared_on_grid(delta_k_array=delta_k, b=confocal_parameter, L=L, z_0=z_0)
log.info(f"J3 calculation finished after {perf_counter() - start} s.")


# %%%
# Calculate nonlinear susceptibility chi3

"Slow"
chi3 = calculate_chi3(
    molecule=molecule,
    paths=paths,
    nu_grid_fund=nu_grid_fund,
    temperature=temperature,
    pressure_self=pressure_self,
    process=process,
    ignore_electronic_state=ignore_electronic_state,
    probe=sfm_probe_wavenumber,
)


# %%%
# Calculate THG/SFM efficiency

"""Parameters: Laser"""
tau = 6 * ureg.ns  # pulse length FWHM
P_1 = 500 * ureg.uJ / tau  # fundamental power
# SFM parameters
probe_power = P_1.m_as(ureg.W)

# Conversion
P_1 = P_1.m_as(ureg.W)
tau = tau.m_as(ureg.s)

if process.startswith("THG"):
    efficiency = THG_efficiency(
        nu=nu_grid_fund,
        absorption_coefficient=absorption_coefficient_signal,
        chi_3=chi3,
        j_3_squared=j3_squared,
        power_fund=P_1,
        absorption_length=L,
    )
    signal_energy = efficiency * P_1 * tau
elif process in (Processes.SFM, Processes.SFM_PERMUTATED):
    # TODO j3 calculation has to be adjusted to different wavelengths
    signal_energy = (
        SFM_power(
            nu_fundamental=nu_grid_fund,
            nu_probe=sfm_probe_wavenumber,
            confocal=2,
            chi_3=chi3,
            j_3_squared=j3_squared,
            power_fundamental=P_1,
            power_probe=probe_power,
            absorption_coefficient=absorption_coefficient_signal,
            absorption_length=L,
        )
        * tau
    )


# %%
# Interpret results

"""Define plots."""


def plot_results(chi3_data, P_data, name: str = "THG") -> None:
    """Plot the results of the simulation."""
    fig, axes = plt.subplots(4, sharex=True, figsize=(7, 10))
    chi3, J3, P, eta = axes  # type: ignore
    axes[-1].set_xlabel("wavenumber (cm$^{-1}$)")  # type: ignore
    chi3.plot(nu_grid_fund, 1e24 * np.abs(chi3_data), label=f"$|\chi^{(3)}|$ {name}")  # noqa: W605,E501  # type: ignore
    chi3.plot(nu_grid_fund, 1e24 * np.real(chi3_data), label="real part")
    chi3.plot(nu_grid_fund, 1e24 * np.imag(chi3_data), label="imaginary part")
    chi3.set_ylabel("$\chi^{(3)}$ (pm²/V²)")  # noqa: W605  # type: ignore
    J3.plot(nu_grid_fund, j3_squared, label="$|J_3|^2$")
    J3.set_ylabel("$J_3$ (1/m²)")
    P.plot(nu_grid_fund, 1e9 * P_data, label="$P_3$")
    P.set_ylabel("pulse energy (nJ)")
    try:
        eta.plot(nu_grid_fund, efficiency, label="$\eta_3$ (THG only)")  # noqa: W605  # type:ignore
        eta.set_ylabel("conversion efficiency")
    except Exception:
        pass
    for ax in axes:  # type: ignore
        ax.legend()


def plot_comparison() -> None:
    plt.figure(figsize=(7, 10))
    ax = plt.gca()
    ax.set_xlabel("wavenumber (cm$^{-1}$)")
    ax.plot(nu_grid_fund, 1e24 * np.abs(chi3), label="THG")
    # ax.plot(nu_grid_fund, 1e24 * np.abs(nonlinear_susceptibility_HCl_SFM), label="SFM")
    ax.set_ylabel("$|\chi^{(3)}|$ (pm²/V²)")  # noqa: W605  # type: ignore
    ax.legend()


def plot_comparison_energy() -> None:
    plt.figure(figsize=(7, 10))
    ax = plt.gca()
    ax.set_xlabel("wavenumber (cm$^{-1}$)")
    ax.plot(nu_grid_fund, 1e9 * np.abs(signal_energy), label="THG")
    # ax.plot(nu_grid_fund, 1e9 * np.abs(SFM_pulse_energy_HCl), label="SFM")
    ax.set_ylabel("pulse energy (nJ)")
    ax.legend()


# %%%
# Plot results

plot_results(chi3, signal_energy)
# plot_results(nonlinear_susceptibility_HCl_SFM, SFM_pulse_energy_HCl, "SFM")


# %%%

plot_comparison()
plot_comparison_energy()


# %%% Save file

def generate_parameters_dict() -> dict:
    return {
        "units": "All units are in SI: Pa, K, m...",
        "process": process.value,
        "temperature": temperature,
        "pressure_self": pressure_self,
        "pressure_air": pressure_air,
        "cell_length": L,
        "confocal_parameter": confocal_parameter,
        "focus_position": z_0,
        "fundamental_power": P_1,
        "pulse_length": tau,
        "ignore_electronic_state": ignore_electronic_state,
        "limits": limits,
    }

# %%%
# Save with generic name

save_spectrum(
    nu_grid_fund=nu_grid_fund,
    chi3=chi3,
    signal_energy=signal_energy,
    j3_squared=j3_squared,
    parameters=generate_parameters_dict(),
    molecule=molecule,
)


# %%%%
# Save with modified name

parameters = generate_parameters_dict()

save_spectrum(
    nu_grid_fund=nu_grid_fund,
    chi3=chi3,
    signal_energy=signal_energy,
    j3_squared=j3_squared,
    parameters=parameters,
    molecule=molecule,
    file_name_suffix=f"{parameters['pressure_self']/100:.0f}mbar",
)



# %%%%
# Load saved data
_name = "sim_data/.pkl"
with open(_name, "rb") as file:
    d = pickle.load(file)
    nu_grid_fund = d["nu"]
    chi3 = d["chi3"]
    signal_energy = d["P"]
    j3_squared = d["J_3"]


# %%
#  Do batch spectrum generation
#################################################################################

for i, kwargs in enumerate((
    {},
    {"ies": True},
    {"process": Processes.SFM},
    {"process": Processes.SFM, "probe": 1e7 / 532},
)):
    pressure = 30
    j_3, chi3, energy, more = calc_all(
        nu_grid_fund=nu_grid_fund,
        molecule=molecule,
        process=kwargs.get("process", Processes.THG),  # type: ignore
        pressure_self_hPa=pressure,
        pressure_air_hPa=0,
        length_cm=18,
        confocal_parameter_cm=6,
        z_0_cm=9,
        pulse_duration_ns=6,
        fundamental_energy_mJ=0.5,
        sfm_probe_wavenumber=kwargs.get("probe", 1e7 / 1064),  # type: ignore
        probe_energy_mJ=0.5,
        temperature_K=temperature,
        lines=lines,
        paths=paths,
        ignore_electronic_state=kwargs.get("ies", False),  # type: ignore
    )
    save_spectrum(
        nu_grid_fund,
        chi3,
        energy,
        j_3,
        parameters=more["parameters"],
        molecule=molecule,
        file_name_suffix=f"{pressure}mbar_fine_{i}",
    )


# %%
# Setup fixed frequency simulation
#################################################################################
"""Start the simulation"""

nu_grid_fund = np.array([2830.3587646484375])  # inv cm

# %%%
# Define temperature and calculate occupation

"""Parameters: Temperature"""
temperature = 273.15 + 25  # Kelvin

# Calculate occupation
calculate_occupation_for_molecule(
    molecule=molecule, temperature=temperature, lines=lines, paths=paths
)

# %%%

# parameters
pressures = np.logspace(-2, 2.5, 100)
process = Processes.THG

# calculation
js = []
chis = []
Es = []
delta_ks = []
for p in pressures:
    j, c, E, a = calc_all(
        nu_grid_fund=nu_grid_fund,
        molecule=molecule,
        process=process,
        pressure_self_hPa=p,
        pressure_air_hPa=0,
        length_cm=18,
        confocal_parameter_cm=6,
        z_0_cm=9,
        pulse_duration_ns=6,
        fundamental_energy_mJ=0.5,
        sfm_probe_wavenumber=1e7 / 1064,
        probe_energy_mJ=0.5,
        temperature_K=temperature,
        lines=lines,
        paths=paths,
    )
    js.append(j)
    chis.append(c)
    Es.append(E)
    delta_ks.append(a.get("delta_k"))


# %%%

plt.figure()
ax = plt.gca()
ax.plot(pressures, np.array(js).flatten())
ax.set_xscale("log")
ax.set_yscale("log")
plt.show()


# %%%

with open(f"sim_data/pressure_dependence_{process}.pkl", "wb") as file:
    pickle.dump(
        (
            pressures,
            np.array(js).flatten(),
            np.array(chis).flatten(),
            np.array(Es).flatten(),
            np.array(delta_ks).flatten(),
        ),
        file,
    )


# %%
# Calculate the phase matching integral
#################################################################################
# does not require anything else than the imports

length = 18e-2  # interaction length in m
confocal_parameter = 6e-2  # confocal parameter in m
z_0 = length / 2  # focus position in m
delta_ks = np.linspace(-100, 100, 600)
js = calc_j3_squared_on_grid(delta_ks, confocal_parameter, length, z_0)

# %%%
plt.figure()
ax = plt.gca()
ax.plot(delta_ks, js)
# ax.set_yscale("log")
ax.set_xlabel("delta_k (1/m)")
ax.set_ylabel("pm integral $|j_3|^2$ (1/m²)")
plt.show()


# %%%
combined = np.stack((delta_ks, js))
np.savetxt(
    f"sim_data/j3_{int(confocal_parameter*100)}.txt",
    combined,
    header=f"phase matching integral |j_3|² (j_3 includes 2/b)\nb={confocal_parameter} m, L={length} m, z_0={z_0} m\ndelta_k in m, |j_3|² in 1/m²",  # noqa: E501
)
