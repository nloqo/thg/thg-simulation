# -*- coding: utf-8 -*-
"""
Calculate the absorption of H20 and of CO2 for a given signal and pump wavelength.

Created on Wed Dec 14 15:19:55 2022

@author: moneke
"""

import os
import pickle

import hapi
import matplotlib.pyplot as plt
import pint
import pyqtgraph as pg


os.chdir(os.path.dirname(os.path.realpath(__file__)))  # change directory to file directory.

hapi.db_begin('data')

ureg = pint.UnitRegistry()

# dictionary of HITRAN keys and their units
units = {
    "nu": 1 / ureg.cm,  # transition vacuum wavenumber
    "a": 1 / ureg.s,  # Einstein-A coefficient
    "gamma_air": 1 / ureg.cm / ureg.atm,  # air-broadened HWHM at T_ref and p_ref
    "gamma_self": 1 / ureg.cm / ureg.atm,  # self-broadened HWHM at T_ref and p_ref
    "e_pp": 1 / ureg.cm,  # lower-state energy of transition. Minimum level of isotopologue is 0.
    "n_air": 1,  # coeffiecient of temperature dependece of air-broadenend HWHM
    "delta_air": 1 / ureg.cm / ureg.atm,  # pressure shift at T_ref and p_ref

    # our own entries:
    "dipole_mom": 1 * ureg.C * ureg.m,  # Dipole moment.
    "2p_res_": 1 / ureg.cm,  # Necessary frequency for 2P resonance
    "delta_1p_bei_2p_res": 1 / ureg.cm,  # Detuning transition frequency - resonance frequency
}
"""Reference values of HITRAN
T_ref = ureg.Quantity(296, ureg.K)
p_ref = 1 ureg.atm
"""


# hapi2.SETTINGS['api_key'] = "9721de51-b0e7-4983-8593-de010e76854a"
# hapi2.fetch_info()
# hapi2.fetch_molecules()

# %% Prepare data (only once)

# %%% Fetch Isotope data

if 0:
    limits = 0, 1e6  # in cm^-1

    hapi.fetch('H2O_1', 1, 1, *limits)

    hapi.fetch('CO2_Isotope1', 2, 1, *limits)
    hapi.fetch('CO2_Isotope2', 2, 2, *limits)


# %%% Calculate absorption_coefficent

path = "sim_data/absorption_coefficients.pkl"
if 0 or not os.path.isfile(path):  # WARNING takes a long time (few minutes), calculate only if necessary.
    limits = 1e7 / 4500, 1e7 / 1300

    ab_co = {}
    ab_co['H2O'] = hapi.absorptionCoefficient_Lorentz(SourceTables="H2O_1",
                                                      HITRAN_units=False,
                                                      WavenumberRange=limits,
                                                      Diluent={'self': 1e-3, 'air': 1 - 1e-3},
                                                      )
    ab_co['CO2'] = hapi.absorptionCoefficient_Lorentz(SourceTables=("CO2_Isotope1", "CO2_Isotope2"),
                                                      HITRAN_units=False,
                                                      WavenumberRange=limits,
                                                      Diluent={'self': 5e-4, 'air': 1 - 5e-4},
                                                      )
    with open(path, "wb") as file:
        pickle.dump(ab_co, file)
else:
    with open(path, "rb") as file:
        ab_co = pickle.load(file)
del path

# %%% Calculate spectra

spectra = {}
for key, value in ab_co.items():
    spectra[key] = hapi.absorptionSpectrum(*value, Environment={'l': 1e-2 if key == "CO2" else 1e1})


# %% Show data inline

def plot_absorption(seed=None, left=None, right=None):
    """Plot absorption profiles.

    :param seed: Seed wavenumber to use to calculate the corresponding signal wavelength of CO2 absorption
    """
    plt.figure(figsize=(7, 10))
    ax = plt.gca()
    ax.plot(*spectra['H2O'], label="H2O")
    if seed is None:
        ax.plot(*spectra['CO2'], label="CO2")
    else:
        ax.plot(seed - spectra['CO2'][0], spectra['CO2'][1], label="CO2 shifted")
    # for key, value in spectra.items():
    #     ax.plot(*value, label=key)
    ax.set_ylabel("absorption over 1 m")
    ax.set_xlim(left=left, right=right)
    ax.set_xlabel("wavenumber (cm^-1)")
    axT = ax.secondary_xaxis('top', functions=(lambda x: 1e7 / x, lambda x: 1e-7 / x))
    axT.set_xlabel("wavelength (nm)")
    ax.legend()


# %%%

plot_absorption()


# %%%

plot_absorption(seed=1e7 / 1064, left=7025, right=7030)


# %% Show data interactive

# Nötig in spyder / jupyter, um die Gui zu aktivieren.
%gui qt
# Create the widget
plotItem = pg.plot()
plotItem.setLabel('bottom', text="fundamental wavenumber (cm^-1)")
plotItem.setLabel('left', text="absorption")
plotItem.addLegend()

lines = {}

for line in ("CO2", "H2O"):
    lines[line] = plotItem.plot([], name=line)
lines['signal'] = plotItem.addLine(0, name="signal")

lines["H20"].setPen("blue")


# %%% CO2

lines["CO2"].setData(spectra['CO2'][0], spectra['CO2'][1])


# %%% H20

seed = 1e7 / 1064.
lines["H20"].setData(seed - spectra['H2O'][0], spectra['H2O'][1])


# %%% Signal

signal = 1e7 / 1419
lines['signal'].setValue(seed - signal)
