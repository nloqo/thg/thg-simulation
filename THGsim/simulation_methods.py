"""Collection of methods used for THG/SFM simulation.
"""

# %%
# standard lib
try:
    from enum import StrEnum  # type: ignore
except ImportError:
    from enum import Enum

    class StrEnum(str, Enum):
        pass
import logging
import os
import pickle
from time import perf_counter
from typing import cast, Optional

# third party packages
try:
    import multiprocess as mp  # enhances standard lib `multiprocessing`  # type: ignore
except ModuleNotFoundError:
    mp = None
import numpy as np
from numpy.typing import NDArray
import pandas as pd
from pandas import DataFrame
from scipy import constants
from scipy.integrate import quad

# local packages
os.chdir(os.path.dirname(os.path.realpath(__file__)))  # change directory to file directory.
try:
    from . import calculations  # noqa: E402
    from . import unit_conversions  # noqa: E402
    from .molecules import BaseMolecule  # noqa: E402
    from .hitran_constants import constantsU, P_REF  # noqa
except ImportError:
    import calculations  # noqa: E402
    import unit_conversions  # noqa: E402
    from molecules import BaseMolecule  # noqa: E402
    from hitran_constants import constantsU, P_REF  # noqa

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())
log.setLevel(logging.INFO)


# %% function definitions
# function definitions


# Methods of simulation
def make_grid(
    df_transitions: DataFrame, lower_nu: float, upper_nu: float, stepsize: float = 0.01
) -> NDArray:
    """Make the meshgrid between lower and upper limit, including all resonance frequencies.

    :param dataframe df_transitions: Data of the transitions.
    :param lower_nu: Lower wavenumber in cm^-1.
    :param upper_nu: Upper wavenumber in cm^-1.
    :param stepsize: Stepsize in cm^-1.
    """
    number_of_steps = int((upper_nu - lower_nu) / stepsize)
    nu_grid_fund = np.linspace(lower_nu, upper_nu, number_of_steps)

    # Add HITRAN transitions to the grid
    nu_hitran_1p = np.unique(
        df_transitions["nu_0_1"][
            (df_transitions["nu_0_1"] > lower_nu) & (df_transitions["nu_0_1"] < upper_nu)
        ].to_numpy()
    )
    nu_hitran_2p = np.unique(
        df_transitions["2p_res_"][
            (df_transitions["2p_res_"] > lower_nu) & (df_transitions["2p_res_"] < upper_nu)
        ].to_numpy()
    )
    nu_hitran_3p = np.unique(
        df_transitions["3p_res_"][
            (df_transitions["3p_res_"] > lower_nu) & (df_transitions["3p_res_"] < upper_nu)
        ].to_numpy()
    )
    nu_grid_fund = np.sort(np.concatenate((nu_grid_fund, nu_hitran_1p, nu_hitran_2p, nu_hitran_3p)))
    return nu_grid_fund


def calc_occupation(
    df: DataFrame,
    molecule: BaseMolecule,
    temperature: float = 298.15,
    statistical_weight: float = 1,
) -> DataFrame:
    """Calculate the ground state population according to given temperature.

    :param dataframe df: Data
    :param temperature: in Kelvin.
    :param molecule: molecule module.
    """
    # calculate unnormalized population:
    df["pop_not_norm"] = calculations.calc_pop(
        df["e_pp"], df["j_pp"], df["g_p"], df["nu"], temperature
    )
    # different function for calculation of the population, doesn't seem to provide correct data
    # here...
    # df["pop_not_norm"] = calculations.calc_pop_HITRAN_version(df['e_pp'], df['j_pp'], df['g_p'],
    #                                                      df['nu'], temperature)

    # Assumption that only the ground state is occupied
    df_for_partition_function = molecule.filter_lines_for_occupation(df)
    # Drop duplicate energies
    df_for_partition_function_no_duplicates = df_for_partition_function.drop_duplicates(
        subset=["e_pp"], inplace=False
    )
    # Calculate the partition functions
    partition_function = df_for_partition_function_no_duplicates["pop_not_norm"].sum()
    # Calculate the vibrational partition functions
    vibrational_partition_function = molecule.vibrational_partition_function(temperature)

    # Calculate total partition functions
    total_partition_function = (
        partition_function * vibrational_partition_function * statistical_weight
    )

    # Calculate normalized occupation
    df["population"] = df["pop_not_norm"] / total_partition_function

    return df


def calc_occupation_transitions(
    df: pd.DataFrame,
    molecule: BaseMolecule,
    temperature: float = 298.15,
    statistical_weight: float = 1,
) -> DataFrame:
    """Calculate the ground state population.

    :param dataframe df: Data
    :param temperature: in Kelvin.
    :param molecule: molecule module.
    """
    # calculate unnormalized population:
    df["pop_not_norm"] = calculations.calc_pop(df["e_pp_0_1"], df["j_pp_0_1"], T=temperature)
    # different function for calculation of the population, doesn't seem to provide correct data
    # here...
    # df["pop_not_norm"] = calculations.calc_pop_HITRAN_version(df['e_pp'], df['j_pp'], df['g_p'],
    #                                                      df['nu'], temperature)

    # Assumption that only the ground state is occupied
    # Drop duplicate energies
    df_for_partition_function_no_duplicates = df.drop_duplicates(subset=["e_pp_0_1"], inplace=False)
    # Calculate the partition functions
    partition_function = df_for_partition_function_no_duplicates["pop_not_norm"].sum()
    # Calculate the vibrational partition functions
    vibrational_partition_function = molecule.vibrational_partition_function(temperature)

    # Calculate total partition functions
    total_partition_function = (
        partition_function * vibrational_partition_function * statistical_weight
    )

    # Calculate normalized occupation
    df["population_0_1"] = df["pop_not_norm"] / total_partition_function

    return df


def calc_linear_susceptibility_on_grid(
    df: DataFrame,
    nu_grid: NDArray,
    temperature: float,
    pressure: float,
    pressure_air: float,
    molecule: BaseMolecule,
    molar_mass: float,
    multiprocess: bool = False,
) -> NDArray:
    """Calculate the linear susceptibility, including the pressure shift.

    According to Boyd (4th edition) 3.2.23:
    chi_ij = N/(epsilon_0 hbar) sum_m (mu_gm^i mu_mg^j) / (omega_mg - omega_p) + (mu_gm^j mu_mg^i) / (omega_mg^* + omega_p)

    with $omega_mg = (E_m - E_g)/hbar - i Gamma_m / 2$ above defined
    
    :param dataframe df: Dataframe containing the transition data.
    :param nu_grid: Grid of the laser wavenumbers in cm^-1.
    :param temperature: in K
    :param pressure: In Pa
    :param pressure_air: pressure of buffer gas (air).
    :param molar_mass: molar mass in kg (contrary to HITRAN!)
    :param bool multiprocess: Use multiprocessing.
    """
    # Calculate pressure shift of resonances
    df_filtered = molecule.filter_lines_for_refractive_index(df)
    if pressure_air > 0:
        nu_array = df_filtered["nu"] + df_filtered["delta_air"] * pressure / P_REF
    else:
        nu_array = df_filtered["nu"]
    omega_transition_array = unit_conversions.vectorized_nu_to_omega(nu_array)
    omega_grid = unit_conversions.vectorized_nu_to_omega(nu_grid)
    population_array = df_filtered["population"]
    dipole_mom_array = df_filtered["dipole_mom"]
    # The natural linewidth is so small, such that it can be neglected (HITRAN)
    natural_linewidth = 0  # * calculations.calc_natural_linewidth(df_filtered["a"])
    gamma_array_pressure_broad = natural_linewidth + unit_conversions.nu_to_omega(
        calculations.vectorized_calc_pressure_broad(
            temperature, df_filtered["n_air"], df_filtered["gamma_air"], pressure_air, pressure, df_filtered["gamma_self"]
        )
    )
    gamma_array_doppler_broad = unit_conversions.nu_to_omega(
        calculations.calc_doppler_broad(nu_array, temperature, molar_mass)
    )
    gamma_array_total_broadening_approx = calculations.calc_total_broadening_approx(
        gamma_array_pressure_broad, gamma_array_doppler_broad
    )
    gamma_array = gamma_array_total_broadening_approx
    # gamma_array = gamma_array_pressure_broad
    mol_density: float = calculations.calc_mol_density(pressure, temperature)
    epsilon_0 = constants.epsilon_0
    hbar = constants.hbar

    def calc_sum_entries(omega):
        """Calculate the entries of the sum with transition/linewidth arrays for a single frequency.
        
        :param omega: light frequency to calculate the entries for.
        """
        term1 = omega_transition_array - 1j * gamma_array - omega  # resonant term
        term2 = omega_transition_array + 1j * gamma_array + omega  # antiresonant term
        return population_array * (dipole_mom_array**2) * (1 / term1 + 1 / term2)

    def calc_linear_susceptibility_worker(omega):
        """Worker function for the pool. Requires imports!"""
        import numpy as np

        tmp = calc_sum_entries(omega)
        return (mol_density / (3 * epsilon_0 * hbar)) * np.sum(tmp)

    if multiprocess:
        with mp.Pool(mp.cpu_count()) as pool:  # type: ignore
            return np.array(pool.map(calc_linear_susceptibility_worker, omega_grid))
    else:
        data = np.zeros_like(omega_grid, dtype=np.complex128)
        for i in range(len(data)):
            tmp = calc_sum_entries(omega_grid[i])
            data[i] = (mol_density / (3 * constants.epsilon_0 * constants.hbar)) * np.sum(tmp)
        return data


def calc_linear_susceptibility_full_molecule(
    nu_grid: NDArray,
    lines: dict[int, DataFrame],
    molecule: BaseMolecule,
    temperature: float,
    pressure_self: float,
    pressure_air: float,
) -> NDArray | float:
    """Calculate the linear susceptibility for a full molecule based on rovibrational transitions.
    """
    value = 0

    for isotope in molecule.isotopes:
        value += (
            calc_linear_susceptibility_on_grid(
                df=lines[isotope],
                nu_grid=nu_grid,
                temperature=temperature,
                pressure=pressure_self,
                pressure_air=pressure_air,
                molecule=molecule,
                molar_mass=molecule.molar_mass[isotope],
            )
            * molecule.abundance[isotope]
        )
        log.debug(f"Isotope {isotope} done.")

    return value


def calc_gauss_profile_on_grid(
    df_transitions, nu_grid, molar_mass, temperature, multiprocess=False
):
    """Calc
    :param bool multiprocess: Use multiprocessing.
    """
    nu_array = df_transitions["nu"]

    def calc_gauss_profile(nu):
        """Worker function for the pool. Requires imports!"""
        import numpy as np
        from scipy import constants

        gamma_doppler_broad = (nu_array / (2.99792458e10)) * np.sqrt(
            2 * constants.N_A * 1.380649e-16 * temperature * np.log(2) / 43.989830
        )
        tmp = np.sqrt(np.log(2) / (constants.pi * gamma_doppler_broad**2)) * np.exp(
            (-np.log(2) * ((nu - nu_array) / gamma_doppler_broad) ** 2)
        )
        return tmp.sum()

    with mp.Pool(mp.cpu_count()) as pool:  # type: ignore
        return np.array(pool.map(calc_gauss_profile, nu_grid))


def calc_refractive_index(linear_susceptibility):
    # ref_index_real = np.real(np.sqrt(1 + linear_susceptibility))
    # ref_index_imag = np.imag(np.sqrt(1 + linear_susceptibility))
    ref_index = np.sqrt(1 + linear_susceptibility)
    return ref_index


def calc_absorption_coefficient(nu, linear_susceptibility):
    return (
        2
        * np.imag(np.sqrt(1 + linear_susceptibility))
        * unit_conversions.nu_to_omega(nu)
        / constants.c
    )


def calc_delta_k(nu_fund, refrac_index_fund, refrac_index_third):
    "Calculate the phase mismatch (Delta k = k_THG - 3 k_F) for THG."
    return (6 * constants.pi / unit_conversions.nu_to_lambda(nu_fund)) * (
        refrac_index_third - refrac_index_fund
    )


def calc_delta_k_fwm(nu_1: float, nu_2: float, nu_3: float, n_1: float, n_2: float, n_3: float, n_sig: float) -> float:
    """Calculate the phase mismatch (Delta k = k_FWM - Sigma k_i) for four wave mixing.

    :param n_i: Refractive index n of wave i (input or the signal)
    :param nu_i: Wavenumber in inverse cm of wave i.
    """
    return 2 * np.pi * (n_sig * (nu_1 + nu_2 + nu_3) - n_1 * nu_1 - n_2 * nu_2 - n_3 * nu_3) * 100


def _calc_j3_squared_on_grid_mp(delta_k_array, b, L, z_0) -> NDArray:
    """Calculate phase matching integral J_3 squared on a grid with multiprocessing."""

    def calc_j3_squared(delta_k):
        from scipy.integrate import quad
        import numpy as np

        def integrand_j3(z, delta_k):
            return (2 / b) * np.exp((-1j * (delta_k) * z)) / (1 + 1j * 2 * (z - z_0) / b) ** 2

        def real_integrand(z):
            return np.real(integrand_j3(z, delta_k))

        def imag_integrand(z):
            return np.imag(integrand_j3(z, delta_k))

        integral_real = quad(real_integrand, 0, L)
        integral_imag = quad(imag_integrand, 0, L)

        abs_quadrat_integral = integral_real[0] ** 2 + integral_imag[0] ** 2
        return abs_quadrat_integral

    with mp.Pool(mp.cpu_count()) as pool:  # type: ignore
        return np.array(pool.map(calc_j3_squared, delta_k_array))


def _calc_j3_squared_on_grid(delta_k_array: NDArray, b: float, L: float, z_0: float) -> NDArray:
    """Calculate squred phase matching integral |J_3|² on a grid."""

    def integrand_j3(z, delta_k):
        """Integrand is in 1/m."""
        return (2 / b) * np.exp(-1j * delta_k * z) / (1 + 1j * 2 * (z - z_0) / b) ** 2

    def real_integrand(z, delta_k):
        return np.real(integrand_j3(z, delta_k))

    def imag_integrand(z, delta_k):
        return np.imag(integrand_j3(z, delta_k))

    data = np.zeros_like(delta_k_array, dtype=float)
    for i, delta_k in enumerate(delta_k_array):
        integral_real = quad(real_integrand, 0, L, delta_k)
        integral_imag = quad(imag_integrand, 0, L, delta_k)

        data[i] = np.real(integral_real[0] ** 2 + integral_imag[0] ** 2)
    return data


def calc_j3_squared_on_grid(
    delta_k_array: NDArray, b: float, L: float, z_0: float, multiprocess: bool = False
) -> NDArray:
    """Calculate squared phase matching integral |J_3|² on a grid.

    The integrand is `(2 / b) * np.exp(-1j * delta_k * z) / (1 + 1j * 2 * (z - z_0) / b) ** 2`.
    The integral over this integrand is dimensionless.

    :param delta_k_array: Array of different phase mismatches.
    :param b: confocal parameter in meters.
    :param L: length of interaction region in meters.
    :param z_0: Focus position in meters.
    :param bool multiprocess: Use multiprocessing.
    """
    if multiprocess:
        return _calc_j3_squared_on_grid_mp(delta_k_array, b, L, z_0)
    else:
        return _calc_j3_squared_on_grid(delta_k_array, b, L, z_0)


class Processes(StrEnum):
    THG = "THG"
    SFM = "SFM"
    THG_PERMUTATED = "THG_PERMUTATED"
    SFM_PERMUTATED = "SFM_PERMUTATED"


def calc_nonlinear_susceptibility_on_grid(
    df_transitions: DataFrame,
    nu_grid_fund: NDArray,
    temperature: float,
    pressure: float,
    molar_mass: float,
    process: str = Processes.THG,
    multiprocess: bool = False,
    electronic: bool = False,
    **kwargs,
) -> NDArray:
    """Calculates nonlinear suszeptibility chi_3 over a grid.

    :param nu_grid_fund: Array of wavenumbers to calculate chi3 for.
    :param temperature: in Kelvin.
    :param pressure: partial pressure in Pascal.
    :param molar_mass: in kilograms.
    :param str process: Which process to calculate?
    :param bool multiprocess: Use multiprocessing.
    :param electronic: Use a hack to supply electronic state information.
    :return: array of nonlinear susceptibility in m^2/V^2
    """
    # TODO Calculate the pressure shift
    # pressure shift macht Vergleich mit Mathematica unmöglich, sollte aber drin sein
    #  df_transitions["nu_0_1"] = df_transitions["nu_0_1"] + df_transitions["delta_air_0_1"] * pressure / 101325  # noqa: E501
    #  df_transitions["nu_1_2"] = df_transitions["nu_1_2"] + df_transitions["delta_air_1_2"] * pressure / 101325  # noqa: E501
    #  df_transitions["nu_2_3"] = df_transitions["nu_2_3"] + df_transitions["delta_air_2_3"] * pressure / 101325  # noqa: E501
    #  df_transitions["nu_0_3"] = df_transitions["nu_0_3"] + df_transitions["delta_air_0_3"] * pressure / 101325  # noqa: E501

    # omega_b_a is transition frequency from state a to b
    omega_b_a_array: pd.Series[float] = unit_conversions.vectorized_nu_to_omega(
        df_transitions["nu_0_1"]
    )  # + df_transitions["e_pp_0_1"] - df_transitions["e_pp_0_1"])
    omega_c_a_array: pd.Series[float] = unit_conversions.vectorized_nu_to_omega(
        df_transitions["nu_1_2"] + df_transitions["e_pp_1_2"] - df_transitions["e_pp_0_1"]
    )
    omega_d_a_array: pd.Series[float] = unit_conversions.vectorized_nu_to_omega(
        df_transitions["nu_2_3"] + df_transitions["e_pp_2_3"] - df_transitions["e_pp_0_1"]
    )

    omega_grid_fund = unit_conversions.vectorized_nu_to_omega(nu_grid_fund)

    """
    Calculation of linewidth: gamma is HWHM of a Lorentz function

    Lorentz form is natural linewidth (mean value of Einstein factors of both levels,
    lowest level has 0) and pressure broadening, Eq. 3.3.25 in Boyd (3. edition)
    """
    gamma_array_pressure_broad_b_a: pd.Series[float] = 0.5 * df_transitions[
        "a_0_1"
    ] + 1 * unit_conversions.nu_to_omega(  # noqa: E501
        calculations.vectorized_calc_pressure_broad(
            temperature,
            df_transitions["n_air_0_1"],
            df_transitions["gamma_air_0_1"],
            0,
            pressure,
            df_transitions["gamma_self_0_1"],
        )
    )
    gamma_array_pressure_broad_d_a: pd.Series[float] = 0.5 * df_transitions[
        "a_0_3"
    ] + 1 * unit_conversions.nu_to_omega(  # noqa: E501
        calculations.vectorized_calc_pressure_broad(
            temperature,
            df_transitions["n_air_0_3"],
            df_transitions["gamma_air_0_3"],
            0,
            pressure,
            df_transitions["gamma_self_0_3"],
        )
    )

    "Doppler broadening"
    # TODO implement transition frequency as parameter for doppler_broad
    # HACK we take the mean laser frequency
    fake_nu: pd.Series[float] = df_transitions["nu_0_1"] * 0 + np.mean(nu_grid_fund)

    gamma_array_doppler_broad: pd.Series[float] = 1 * unit_conversions.vectorized_nu_to_omega(
        calculations.calc_doppler_broad(fake_nu, temperature, molar_mass)
    )
    # gamma_array_doppler_broad_b_a = 1 * unit_conversions.nu_to_omega(
    #     calculations.calc_doppler_broad(fake_nu, temperature, molar_mass))
    # gamma_array_doppler_broad_c_a = 1 * unit_conversions.nu_to_omega(
    #     calculations.calc_doppler_broad(fake_nu, temperature, molar_mass))
    # gamma_array_doppler_broad_d_a = 1 * unit_conversions.nu_to_omega(
    #     calculations.calc_doppler_broad(df_transitions["nu_0_3"], temperature, molar_mass))

    "Total linewith: "
    gamma_array_total_broadening_approx_b_a = calculations.calc_total_broadening_approx(
        gamma_pressure_broadened=gamma_array_pressure_broad_b_a,
        gamma_doppler_broadened=gamma_array_doppler_broad,
    )
    gamma_array_total_broadening_approx_d_a = calculations.calc_total_broadening_approx(
        gamma_pressure_broadened=gamma_array_pressure_broad_d_a,
        gamma_doppler_broadened=gamma_array_doppler_broad,
    )
    # HACK Linienbreite von c durch Mittelung der Linienbreiten von b und d genähert: gültig?
    if electronic:
        # HACK as 0_3 is the electronic transition, we have to estimate its values,
        # based on 0_1 and 1_2
        # HACK just use the linewidth of the first line
        gamma_array_total_broadening_approx_d_a_mod = calculations.calc_total_broadening_approx(
            gamma_pressure_broadened=gamma_array_pressure_broad_b_a,
            gamma_doppler_broadened=gamma_array_doppler_broad,
        )
        gamma_array_total_broadening_approx_c_a = (
            gamma_array_total_broadening_approx_b_a + gamma_array_total_broadening_approx_d_a_mod
        ) / 2
    else:
        gamma_array_total_broadening_approx_c_a = (
            gamma_array_total_broadening_approx_b_a + gamma_array_total_broadening_approx_d_a
        ) / 2

    gamma_array_b_a = gamma_array_total_broadening_approx_b_a
    gamma_array_c_a = gamma_array_total_broadening_approx_c_a
    gamma_array_d_a = gamma_array_total_broadening_approx_d_a

    dipole_mom_b_a_array: pd.Series[float] = df_transitions["dipole_mom_0_1"]
    dipole_mom_c_b_array: pd.Series[float] = df_transitions["dipole_mom_1_2"]
    dipole_mom_d_c_array: pd.Series[float] = df_transitions["dipole_mom_2_3"]
    dipole_mom_d_a_array: pd.Series[float] = df_transitions["dipole_mom_0_3"]

    population_a_array: pd.Series[float] = df_transitions["population_0_1"]

    N = calculations.calc_mol_density(pressure, temperature)

    epsilon_0 = constants.epsilon_0
    hbar = constants.hbar

    def calc_nonlinear_susceptibility_THG_permutated(omega: float, **kwargs) -> complex:
        """With all the permutations."""
        import numpy as np

        term_1 = 1 / (
            (omega_d_a_array - 3 * omega - 1j * gamma_array_d_a)
            * (omega_c_a_array - 2 * omega - 1j * gamma_array_c_a)
            * (omega_b_a_array - 1 * omega - 1j * gamma_array_b_a)
        )
        term_2 = 1 / (
            (omega_d_a_array + 1 * omega + 1j * gamma_array_d_a)
            * (omega_c_a_array - 2 * omega - 1j * gamma_array_c_a)
            * (omega_b_a_array - 1 * omega - 1j * gamma_array_b_a)
        )
        term_3 = 1 / (
            (omega_d_a_array + 1 * omega + 1j * gamma_array_d_a)
            * (omega_c_a_array + 2 * omega + 1j * gamma_array_c_a)
            * (omega_b_a_array - 1 * omega - 1j * gamma_array_b_a)
        )
        term_4 = 1 / (
            (omega_d_a_array + 1 * omega + 1j * gamma_array_d_a)
            * (omega_c_a_array + 2 * omega + 1j * gamma_array_c_a)
            * (omega_b_a_array + 3 * omega + 1j * gamma_array_b_a)
        )
        tmp = (
            population_a_array
            * dipole_mom_b_a_array
            * dipole_mom_c_b_array
            * dipole_mom_d_c_array
            * dipole_mom_d_a_array
            * (term_1 + term_2 + term_3 + term_4)
        )
        nonlinear_susceptibility = np.sum(tmp) * N / (epsilon_0 * hbar**3)
        return nonlinear_susceptibility

    def calc_nonlinear_susceptibility_THG(omega, **kwargs) -> complex:
        """Just the first term."""
        import numpy as np

        term_1 = 1 / (
            (omega_d_a_array - 3 * omega - 1j * gamma_array_d_a)
            * (omega_c_a_array - 2 * omega - 1j * gamma_array_c_a)
            * (omega_b_a_array - 1 * omega - 1j * gamma_array_b_a)
        )
        tmp = (
            population_a_array
            * dipole_mom_b_a_array
            * dipole_mom_c_b_array
            * dipole_mom_d_c_array
            * dipole_mom_d_a_array
            * term_1
        )
        nonlinear_susceptibility = np.sum(tmp) * N / (epsilon_0 * hbar**3)
        return nonlinear_susceptibility

    def calc_nonlinear_susceptibility_SFM(
        omega, probe=unit_conversions.nu_to_omega(1e7 / 1064), **kwargs
    ) -> complex:
        """Just the most simple approach."""
        import numpy as np

        term_1 = 1 / (
            (omega_d_a_array - 2 * omega - probe - 1j * gamma_array_d_a)
            * (omega_c_a_array - 2 * omega - 1j * gamma_array_c_a)
            * (omega_b_a_array - 1 * omega - 1j * gamma_array_b_a)
        )
        tmp = (
            population_a_array
            * dipole_mom_b_a_array
            * dipole_mom_c_b_array
            * dipole_mom_d_c_array
            * dipole_mom_d_a_array
            * term_1
        )
        return np.sum(tmp) * N / (epsilon_0 * hbar**3)

    def calc_nonlinear_susceptibility_SFM_permutated(
        omega, probe=unit_conversions.nu_to_omega(1e7 / 1064), **kwargs
    ) -> complex:
        """Average over the wavelength permutations."""
        import numpy as np

        term_1 = 1 / (
            (omega_d_a_array - 2 * omega - probe - 1j * gamma_array_d_a)
            * (omega_c_a_array - 2 * omega - 1j * gamma_array_c_a)
            * (omega_b_a_array - omega - 1j * gamma_array_b_a)
        )
        term_2 = 1 / (
            (omega_d_a_array - 2 * omega - probe - 1j * gamma_array_d_a)
            * (omega_c_a_array - omega - probe - 1j * gamma_array_c_a)
            * (omega_b_a_array - omega - 1j * gamma_array_b_a)
        )
        term_3 = 1 / (
            (omega_d_a_array - 2 * omega - probe - 1j * gamma_array_d_a)
            * (omega_c_a_array - omega - probe - 1j * gamma_array_c_a)
            * (omega_b_a_array - probe - 1j * gamma_array_b_a)
        )
        tmp = (
            population_a_array
            * dipole_mom_b_a_array
            * dipole_mom_c_b_array
            * dipole_mom_d_c_array
            * dipole_mom_d_a_array
            * (term_1 + term_2 + term_3)
            / 3
        )
        return np.sum(tmp) * N / (epsilon_0 * hbar**3)

    if process == Processes.THG_PERMUTATED:
        calc_nonlinear_susceptibility = calc_nonlinear_susceptibility_THG_permutated
    elif process == Processes.SFM:
        calc_nonlinear_susceptibility = calc_nonlinear_susceptibility_SFM
    elif process == Processes.THG:
        calc_nonlinear_susceptibility = calc_nonlinear_susceptibility_THG
    elif process == Processes.SFM_PERMUTATED:
        calc_nonlinear_susceptibility = calc_nonlinear_susceptibility_SFM_permutated
    else:
        raise ValueError("Invalid process name '{process}' given.")

    if multiprocess:
        with mp.Pool(mp.cpu_count()) as pool:  # type: ignore
            return np.array(pool.map(calc_nonlinear_susceptibility, omega_grid_fund))
    else:
        data = np.zeros_like(omega_grid_fund, dtype=np.complex128)
        for i, omega in enumerate(omega_grid_fund):
            data[i] = calc_nonlinear_susceptibility(omega, **kwargs)
        return data


def THG_efficiency(
    nu, absorption_coefficient, chi_3, j_3_squared, power_fund: float, absorption_length: float
):
    """Calculate the THG conversion efficiency (P_THG/P_f)."""
    return (
        (3 / 4)
        * (constants.mu_0**2)
        * (unit_conversions.nu_to_omega(nu) / unit_conversions.nu_to_lambda(nu)) ** 2
        * np.abs(chi_3) ** 2
        * j_3_squared
        * power_fund**2
        * np.exp(-absorption_coefficient * absorption_length)
    )


def SFM_power(
    nu_fundamental,
    nu_probe: float,
    confocal: float,
    chi_3,
    j_3_squared,
    power_fundamental: float,
    power_probe: float,
    absorption_coefficient,
    absorption_length: float = 0,
):
    """Calculate the SFM power in Watts from the parameters.

    :param nu_fundamental: Fundamental wavenumber in cm^-1.
    :param nu_probe: Probe wavenumber in cm^-1.
    :param confocal: Confocal parameter in m. Set it to "2", if J_3 already contains it.
    :param chi_3: complex value (or absolute value) of nonlinear susceptibility chi_3 in m^2/V^2.
    :param j_3_squared: Squared absolute value of phase matching integral J_3
        which DOES NOT INCLUDE 2/b!
    :param power_x: Power of beam x in Watts.
    :param absorption_coefficient: in optical depth per meter.
    :param length: Absorption length in meters.
    """
    return (
        (2 * np.pi * constants.c * 100) ** 4
        * (2 * nu_fundamental + nu_probe)
        * nu_fundamental**2
        * nu_probe
        / (4 * np.pi**2 * constants.epsilon_0**2 * constants.c**6 * confocal**2)
        * np.abs(chi_3) ** 2
        * j_3_squared
        * power_fundamental**2
        * power_probe
        * np.exp(-absorption_coefficient * absorption_length)
    )


# %%
# Define simulation functions


def calculate_occupation_for_molecule(
    molecule: BaseMolecule,
    temperature: float,
    lines: dict[int, DataFrame],
    paths: Optional[dict[int, DataFrame]],
) -> None:
    """Calculate the occupation and write it into the `lines` and `paths` dataframe lists"""
    for isotope in molecule.isotopes:
        # Add the occupation to all lines
        calc_occupation(df=lines[isotope], temperature=temperature, molecule=molecule)
        # Modify the transition sets
        if paths is not None:
            calc_occupation_transitions(
                df=paths[isotope], molecule=molecule, temperature=temperature
            )


def calculate_absorption_and_phase_mismatch(
    nu_grid_fund: np.ndarray,
    lines: dict[int, DataFrame],
    molecule: BaseMolecule,
    pressure_self: float,
    pressure_air: float,
    temperature: float = 273.15,
    process: Processes = Processes.THG,
    sfm_probe_wavenumber: Optional[float] = None,
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Calculate the absorption coefficients and the phase mismatch.

    Slow!
    :return: fundamental absorption, signal absorption, phase mismatch in 1/m
    """
    if process in (Processes.THG, Processes.THG_PERMUTATED):
        nu_grid_signal = 3 * nu_grid_fund
    elif process in (Processes.SFM, Processes.SFM_PERMUTATED) and sfm_probe_wavenumber is not None:
        nu_grid_signal = 2 * nu_grid_fund + sfm_probe_wavenumber
    else:
        raise ValueError("Unknown process")

    start = perf_counter()
    linear_susceptibility_fund = calc_linear_susceptibility_full_molecule(
        nu_grid=nu_grid_fund,
        lines=lines,
        molecule=molecule,
        temperature=temperature,
        pressure_self=pressure_self,
        pressure_air=pressure_air,
    )
    linear_susceptibility_signal = calc_linear_susceptibility_full_molecule(
        nu_grid=nu_grid_signal,
        lines=lines,
        molecule=molecule,
        temperature=temperature,
        pressure_self=pressure_self,
        pressure_air=pressure_air,
    )
    log.info(f"Linear susceptibility finished after {perf_counter() - start} s.")

    # as above only uses rovibrational transitions, we have to add the effect from electronic states
    # based on literature values:

    n_fund = molecule.modify_ref_index_fund(
                np.sqrt(1 + linear_susceptibility_fund),
                nu_grid_fund,
                pressure_self,
                temperature,
            )
    n_signal = molecule.modify_ref_index_signal(
                np.sqrt(1 + linear_susceptibility_signal),
                nu_grid_signal,
                pressure_self,
                temperature,
            )

    # Calculate absorption
    absorption_coefficient_fund = calc_absorption_coefficient(
        nu_grid_fund, linear_susceptibility_fund
    )
    absorption_coefficient_signal = calc_absorption_coefficient(
        nu_grid_signal, linear_susceptibility_signal
    )

    # Calculate phase mismatch
    if process in (Processes.THG, Processes.THG_PERMUTATED):
        vectorized_delta_k = np.vectorize(calc_delta_k)
        delta_k = vectorized_delta_k(
            nu_grid_fund,
            n_fund,
            n_signal,
        )
    elif process in (Processes.SFM, Processes.SFM_PERMUTATED):
        vectorized_delta_k = np.vectorize(calc_delta_k_fwm)
        delta_k = vectorized_delta_k(
            nu_grid_fund,
            nu_grid_fund,
            sfm_probe_wavenumber,
            n_fund,
            n_fund,
            molecule.modify_ref_index_signal(1, sfm_probe_wavenumber, pressure_self, temperature),
            n_signal,
        )
    return absorption_coefficient_fund, absorption_coefficient_signal, delta_k


def calculate_chi3(
    molecule: BaseMolecule,
    paths: dict[int, pd.DataFrame],
    nu_grid_fund: np.ndarray,
    temperature: float,
    pressure_self: float,
    process: Processes,
    ignore_electronic_state: bool = False,
    **kwargs,
) -> NDArray | float:
    """Calculate the nonlinear susceptibility third order in m^2/V^2"""
    chi3 = 0  # start value
    for isotope in molecule.isotopes:
        start = perf_counter()
        isotope_paths = paths[isotope]
        if ignore_electronic_state:
            # it has to be equal comparison due to dataframe, 'is' is not valid comparison here!
            isotope_paths = cast(DataFrame, isotope_paths[isotope_paths["el_0"] == False])  # type: ignore  # noqa: E712
        chi3 += (
            calc_nonlinear_susceptibility_on_grid(
                df_transitions=isotope_paths,
                nu_grid_fund=nu_grid_fund,
                temperature=temperature,
                pressure=pressure_self,
                molar_mass=molecule.molar_mass[isotope],
                process=process,
                **kwargs,
            )
            * molecule.abundance[isotope]
        )
        log.info(
            f"Chi3 calculation finished for isotope {isotope} after {perf_counter() - start} s."
        )
    return chi3


def calc_all(
    nu_grid_fund,
    molecule: BaseMolecule,
    process: Processes,
    temperature_K: float,
    pressure_self_hPa: float,
    pressure_air_hPa: float,
    length_cm: float,
    confocal_parameter_cm: float,
    z_0_cm: float,
    pulse_duration_ns: float,
    fundamental_energy_mJ: float,
    lines: dict[int, DataFrame],
    paths: dict[int, DataFrame],
    ignore_electronic_state: bool = False,
    sfm_probe_wavenumber: Optional[float] = None,
    probe_energy_mJ: Optional[float] = None,
):
    """Calculate the whole simulation in one method (for batch work).

	The calculation assumes a rectangular temporal pulse shape.
	If you have a Gaussian pulse, divide the result by 2.

    :return: |J_3|², chi^(3) in m²/V², signal_energy in J, dict of more
    """
    pressure_self = 100 * pressure_self_hPa  # in Pa
    pressure_air = 100 * pressure_air_hPa  # in Pa
    L = length_cm * 1e-2  # in m
    b = confocal_parameter_cm * 1e-2  # in m
    z_0 = z_0_cm * 1e-2  # in m
    tau = pulse_duration_ns * 1e-9  # FWHM in s
    P_1 = fundamental_energy_mJ * 1e-3 / tau  # peak power rectangular in W
    probe_power = None if probe_energy_mJ is None else probe_energy_mJ * 1e-3 / tau

    # update the lines and paths data with the temperature, if it has changed.
    global _last_temperature
    try:
        last_temp = _last_temperature  # type: ignore
    except NameError:
        last_temp = 0
    if temperature_K != last_temp:
        calculate_occupation_for_molecule(
            molecule=molecule, temperature=temperature_K, lines=lines, paths=paths
        )
        _last_temperature = temperature_K

    absorption_coefficient_fund, absorption_coefficient_signal, delta_k = (
        calculate_absorption_and_phase_mismatch(
            nu_grid_fund=nu_grid_fund,
            lines=lines,
            molecule=molecule,
            pressure_self=pressure_self,
            pressure_air=pressure_air,
            process=process,
            sfm_probe_wavenumber=sfm_probe_wavenumber,
        )
    )

    # Calculate phase matching integral j3 (absolute value)

    "Slow"
    # start = perf_counter()
    j3_squared = calc_j3_squared_on_grid(delta_k_array=delta_k, b=b, L=L, z_0=z_0)
    # log.info(f"J3 calculation finished after {perf_counter() - start} s.")

    # Calculate nonlinear susceptibility chi3

    "Slow"
    chi3 = calculate_chi3(
        molecule=molecule,
        paths=paths,
        nu_grid_fund=nu_grid_fund,
        temperature=temperature_K,
        pressure_self=pressure_self,
        process=process,
        ignore_electronic_state=ignore_electronic_state,
        probe=sfm_probe_wavenumber,
    )

    # Calculate THG/SFM efficiency

    if process.startswith("THG"):
        efficiency = THG_efficiency(
            nu=nu_grid_fund,
            absorption_coefficient=absorption_coefficient_signal,
            chi_3=chi3,
            j_3_squared=j3_squared,
            power_fund=P_1,
            absorption_length=L,
        )
        signal_energy = efficiency * P_1 * tau
    elif process in (Processes.SFM, Processes.SFM_PERMUTATED):
        signal_energy = (
            SFM_power(
                nu_fundamental=nu_grid_fund,
                nu_probe=sfm_probe_wavenumber,  # type: ignore
                confocal=2,
                chi_3=chi3,
                j_3_squared=j3_squared,
                power_fundamental=P_1,
                power_probe=probe_power,  # type: ignore
                absorption_coefficient=absorption_coefficient_signal,
                absorption_length=L,
            )
            * tau
        )
    parameters: dict = {
            "units": "All units are in SI: Pa, K, m...",
            "process": process.value,
            "temperature": temperature_K,
            "pressure_self": pressure_self,
            "pressure_air": pressure_air,
            "cell_length": L,
            "confocal_parameter": b,
            "focus_position": z_0,
            "fundamental_power": P_1,
            "pulse_length": tau,
            "ignore_electronic_state": ignore_electronic_state,
            # "limits": limits,
        }
    additional = {"delta_k": delta_k, "parameters": parameters}
    return j3_squared, chi3, signal_energy, additional


def load_stored_lines_and_paths(
    molecule: BaseMolecule,
) -> tuple[dict[int, DataFrame], dict[int, DataFrame]]:
    """Load the already calculated lines and transition paths from files."""
    # HITRAN line data
    lines = {}  # all absorption lines

    for isotope in molecule.isotopes:
        lines[isotope] = pd.read_pickle(f"sim_data/{molecule.name}_{isotope}_lines.pkl")

    # Load stored transition paths
    paths: dict[int, DataFrame] = {}  # transition paths
    for isotope in molecule.isotopes:
        paths[isotope] = pd.read_pickle(f"sim_data/{molecule.name}_{isotope}_paths.pkl")

    return lines, paths


def save_spectrum(
    nu_grid_fund,
    chi3,
    signal_energy,
    j3_squared,
    parameters: dict,
    molecule: BaseMolecule,
    file_name_suffix: str = "",
) -> str:
    """Save a spectrum and return the file name."""
    def generate_data_dict() -> dict:
        return {
            "nu": nu_grid_fund,
            "chi3": chi3,
            "P": signal_energy,
            "J_3": j3_squared,
            "parameters": parameters,
        }

    process = parameters.get("process", "unknown")
    # Save with generic name
    _name = (
        f"sim_data/{molecule.name}_{process}{'_' if file_name_suffix else ''}{file_name_suffix}.pkl"
    )
    with open(_name, "wb") as file:
        pickle.dump(generate_data_dict(), file)
    print(_name)
    return _name


# %%
