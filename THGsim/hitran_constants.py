import pint
from scipy import constants

ureg = pint.UnitRegistry()

# dictionary of HITRAN keys and their units
units = {
    "nu": 1 / ureg.cm,  # transition vacuum wavenumber
    "a": 1 / ureg.s,  # Einstein-A coefficient
    "gamma_air": 1 / ureg.cm / ureg.atm,  # air-broadened HWHM at T_ref and p_ref
    "gamma_self": 1 / ureg.cm / ureg.atm,  # self-broadened HWHM at T_ref and p_ref
    "e_pp": 1 / ureg.cm,  # lower-state energy of transition. Minimum level of isotopologue is 0.
    "n_air": 1,  # coefficient of temperature dependence of air-broadenend HWHM
    "delta_air": 1 / ureg.cm / ureg.atm,  # pressure shift at T_ref and p_ref
    # our own entries:
    "dipole_mom": 1 * ureg.C * ureg.m,  # Dipole moment.
    "2p_res_": 1 / ureg.cm,  # Necessary frequency for 2P resonance
    "delta_1p_bei_2p_res": 1 / ureg.cm,  # Detuning transition frequency - resonance frequency
}
"""Reference values of HITRAN
T_ref = ureg.Quantity(296, ureg.K)
p_ref = 1 ureg.atm
"""
# Reference values
T_REF = 296  # K
P_REF = 101325  # Pa == 1 atm


class constantsU:
    """Constants in units"""

    c = constants.c * ureg.m / ureg.s
    h = constants.h * ureg.J * ureg.s
    hbar = constants.hbar * ureg.J * ureg.s
    epsilon_0 = constants.epsilon_0 * ureg.A * ureg.s / ureg.V / ureg.m
    k = constants.k * ureg.J / ureg.K
