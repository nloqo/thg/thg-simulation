"""Often used unit conversions"""

import numpy as np
from scipy import constants


def nu_to_freq(nu):
    """
    Converts the wavenumber nu in cm^-1 to frequency f in s^-1.

            Parameters:
                    nu (float): Wavenumber in in cm^-1

            Returns:
                    freq (float): Frequency in s^-1
    """
    return nu * 100 * constants.c


vectorized_nu_to_freq = np.vectorize(nu_to_freq)


def nu_to_freq_bandwidth(nu, delta_nu):
    """
    Converts the wavenumber nu in cm^-1 to frequency f in s^-1.

            Parameters:
                    nu (float): Wavenumber in in cm^-1

            Returns:
                    freq (float): Frequency in s^-1
    """
    return nu_to_freq(nu + delta_nu / 2) - nu_to_freq(nu - delta_nu / 2)


vectorized_nu_to_freq_bandwidth = np.vectorize(nu_to_freq_bandwidth)


def nu_to_omega(nu: float) -> float:
    """
    Converts the wavenumber nu in cm^-1 to angular frequency omega in s^-1.

            Parameters:
                    nu (float): Wavenumber in in cm^-1

            Returns:
                    omega (float): Angular frequency in s^-1
    """
    return nu * 100 * constants.c * 2 * np.pi


vectorized_nu_to_omega = np.vectorize(nu_to_omega)


def nu_to_lambda(nu: float) -> float:
    """
    Converts the wavenumber nu in cm^-1 to wavelength lambda in m.

            Parameters:
                    nu (float): Wavenumber in in cm^-1

            Returns:
                    lambda (float): Wavelength lambda in m
    """
    return 1 / (nu * 100)


vectorized_nu_to_lambda = np.vectorize(nu_to_lambda)


def lambda_to_nu(lam: float) -> float:
    """
    Converts the wavelength lambda in m to wavenumber nu in cm^-1.

            Parameters:
                    lambda (float): Wavelength lambda in m

            Returns:
                    nu (float): Wavenumber nu in cm^-1
    """
    return 1 / (lam * 100)


def nu_to_lambda_in_nm(nu: float) -> float:
    """
    Converts the wavenumber nu in cm^-1 to wavelength lambda in nm.

            Parameters:
                    lambda (float): Wavenumber nu in cm^-1

            Returns:
                    nu (float): Wavelength lambda in nm
    """
    return 10**9 / (nu * 100)


vectorized_nu_to_lambda = np.vectorize(nu_to_lambda)


def lambda_in_nm_to_nu(lam: float) -> float:
    """
    Converts the wavelength lambda in nm to wavenumber nu in cm^-1.

            Parameters:
                    lambda (float): Wavelength lambda in nm

            Returns:
                    nu (float): Wavenumber nu in cm^-1
    """
    return 1e7 / lam


def energy_nu_to_energy_joule(energy_nu: float) -> float:
    """
    Converts the energy in cm^-1 to the energy in Joule.

            Parameters:
                    energy_nu (float): Energy in cm^-1

            Returns:
                    energy_joule (float): Energy in J
    """
    return constants.h * energy_nu * 100 * constants.c


vectorized_energy_nu_to_energy_joule = np.vectorize(energy_nu_to_energy_joule)


def refractive_index_to_wavevector(refractive_index: float, nu: float) -> float:
    """Convert refractive index to wavevector k"""
    return refractive_index * nu_to_omega(nu) / constants.c
