"""Spectroscopic calculations based on HITRAN"""

import numpy as np
from scipy import constants

try:
    from . import unit_conversions
    from .hitran_constants import P_REF, T_REF
except ImportError:
    import unit_conversions
    from hitran_constants import P_REF, T_REF


def calc_pressure_shift(nu: float, delta_air: float, p: float) -> float:
    """Calculate the shift of the wavenumber (in cm^-1) at a given pressure.

    HITRAN: "In fact, the availability of shift values is still sparse, and the experimental values have high
    uncertainties associated with them."
    Source HITRAN page: https://hitran.org/docs/definitions-and-units/

    :param nu: wavenumber in cm^-1
    :param delta_air: shift value from HITRAN
    :param p: pressure in Pa
    :return: shift in cm^-1    
    """
    return nu + delta_air * p / P_REF


vectorized_calc_pressure_shift = np.vectorize(calc_pressure_shift)


# Calculate the dipole moment
def calc_dipole_moment(nu: float, einstein_a: float) -> float:
    """
    Calculate the dipole moment.

    :param nu: wavenumber in cm^-1
    :param einstein_a: Einstein coefficien in 1/s (e.g. from HITRAN)
    :return: dipole moment in C/m
    """
    return np.sqrt(
        3
        * constants.epsilon_0
        * constants.h
        * constants.c**3
        * einstein_a
        / (2 * unit_conversions.nu_to_omega(nu) ** 3)  # == 2 * (2 pi f)**3
    )


# Calculate the molecular density
def calc_mol_density(p: float, T: float) -> float:
    """
    Calculates the molecular density.

            Parameters:
                    p (float): Pressure in Pa
                    T (float): Temperature in K

            Returns:
                    N (float): Molecular density in 1 / m^3
    """
    return p / (constants.k * T)


# Calculate upper vibrational quantum number j_p and vectorize
def calc_j_p(br: str, j_pp: int) -> int:
    """
    Calculate the upper vibrational quantum number j_p.

    :param branch: Branch name
    :param j_pp: Lower vibrational quantum number
    :return: j_p, the upper vibrational quantum number
    """
    if br == 'P':
        j_p = int(j_pp) - 1
    elif br == 'R':
        j_p = int(j_pp) + 1
    elif br == 'Q':
        j_p = int(j_pp) + 0
    else:
        raise ValueError()
    return j_p


vectorized_calc_j_p = np.vectorize(calc_j_p)


def calc_pop(e_pp, j_pp, g_p=None, nu=None, T=273.15):
    """Calculate the (not normalized) population distribution according to Boltzmann.

    The calculation includes the rotational substates.

    :param e_pp: energy of the lower state in cm^-1.
    :param j_pp: rotational quantum number of the lower state.
    :param g_p: ? not used
    :param nu: ? not used
    :param T: Temperature in Kelvin.
    :returns: ???
    """
    return (2 * j_pp + 1) * np.exp(
        -unit_conversions.energy_nu_to_energy_joule(e_pp) / (constants.k * T)
    )


def calc_pop_HITRAN_version(e_pp, j_pp, g_p, nu, t):
    """Calculate the population according to HITRAN."""
    return g_p * np.exp(- 1.4387769 * (e_pp) / (t)) * (1 - np.exp(- 1.4387769 * (nu) / (t)))


vectorized_calc_pop = np.vectorize(calc_pop)


def calc_partition_function_term_HITRAN_version(e_pp: float, t: float) -> float:
    return np.exp(-1.4387769 * (e_pp) / (t))


def calc_e_p(nu: float, e_pp: float) -> float:
    """
    Calculate the upper energy.

    :param nu: Wavenumber in cm^-1
    :param e_pp: Lower energy in cm^-1
    :return: e_p, upper energy in cm^-1
    """
    return nu + e_pp


vectorized_calc_e_p = np.vectorize(calc_e_p)


def calc_natural_linewidth(einstein_a):
    """Calculate the natural linewidth HWHM in cm^-1 from Einstein A coefficient in 1/s."""
    return einstein_a / (2 * np.pi) / 2 / constants.c * 1e-2


def calc_pressure_broad(
    temperature: float,
    n_air: float,
    gamma_air: float,
    p_air: float,
    p_self: float,
    gamma_self: float,
) -> float:
    """Calculate the (Lorentzian) gamma (HWHM) with pressure broadening.

    Source https://hitran.org/docs/definitions-and-units/

    :param temperature: in Kelvin
    :params n_air, gamma_air, gamma_self: values from HITRAN
    :param p_self: partial pressure in Pa.
    :param p_air: air pressure excluding own pressure in Pa.
    """
    return (T_REF / temperature) ** n_air * (
        (gamma_air * p_air / P_REF) + (gamma_self * p_self / P_REF)
    )


vectorized_calc_pressure_broad = np.vectorize(calc_pressure_broad)


def calc_doppler_broad(nu, temperature: float, molar_mass: float) -> float:
    """Calculate the (Gaussian) HWHM of Doppler broadening

    https://hitran.org/docs/definitions-and-units/,
    [5] J. Tennyson et al., "Recommended isolated-line profile for representing
    high-resolution spectroscopic transitions (IUPAC Technical Report)",
    Pure Appl. Chem. 86, 1931-1943 (2014). https://doi.org/10.1515/pac-2014-0208 [ADS]

    :param nu: Wavenumber of the radiation in cm^-1
    :param temperature: in Kelvin
    :param molar_mass: in kilograms per mol (contrary to HITRAN!)
    """
    return nu / constants.c * np.sqrt(
        2 * np.log(2) * constants.k * temperature * constants.N_A / molar_mass)


def calc_total_broadening_approx(gamma_pressure_broadened, gamma_doppler_broadened):
    """Calculates the HWHM with pressure and doppler broadening"""
    return 0.5346 * (gamma_pressure_broadened) + np.sqrt(
        0.2166 * (gamma_pressure_broadened) ** 2 + (gamma_doppler_broadened) ** 2
    )
