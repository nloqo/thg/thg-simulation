"""Calculate the transition transition paths for a molecule.

This module allows to calculate possible three photon excitation paths
for a specific molecule.
As these calculations take some time, the results are stored in pickled files per isotope.

Call this file with the molecule name (an appropriate :class:`Molecule` class has to be defined), for example:
:code:`python3 calculate_transition_paths.py HCl`
"""

import argparse
import logging
import gc
import importlib
import os
from time import perf_counter

import numpy as np
import pandas as pd
from pandas import DataFrame

# local packages
os.chdir(os.path.dirname(os.path.realpath(__file__)))  # change directory to file directory.
import calculations  # noqa: E402
from molecules import BaseMolecule  # noqa: E402


log = logging.getLogger(__name__)
if not log.handlers:
    log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)


def main() -> None:
    """Setup the necessary files for the simulation.

    Change the imported molecule to your molecule
    """
    parser = argparse.ArgumentParser(
        description=("Calculate transitions for a molecule and store the data in a file per isotope."
                     " This calculation is time consuming (several minutes).")
    )
    parser.add_argument(
        "molecule",
        help="Name of the molecule to load from molecules directory.",
    )
    kwargs = vars(parser.parse_args())
    molecule_name = kwargs["molecule"]

    mol_module = importlib.import_module(f"molecules.{molecule_name}")
    molecule_class = getattr(mol_module, molecule_name)

    molecule: BaseMolecule = molecule_class()

    lines = lines_from_hitran_data(molecule)

    calculate_transition_paths(molecule=molecule, lines=lines)


def lines_from_hitran_data(molecule: BaseMolecule) -> dict[int, DataFrame]:
    """Get the lines information from hitran data and store it in a file per isotope."""
    import hapi
    hapi.db_begin("data")
    for isotope in molecule.isotopes:
        # last parameters are numin, numax
        hapi.fetch(
            TableName=f"{molecule.name}_{isotope}",
            M=molecule.id,
            I=isotope,
            numin=molecule.nu_range[0],
            numax=molecule.nu_range[1],
        )

    # Collect transitions

    lines: dict[int, DataFrame] = {}

    for isotope in molecule.isotopes:
        lines[isotope] = molecule.interpret_transitions(
            hapi=hapi, table_name=f"{molecule.name}_{isotope}"
        )
        lines[isotope]["j_p"] = calculations.vectorized_calc_j_p(
            lines[isotope]["branch"], lines[isotope]["j_pp"]
        )
        lines[isotope]["dipole_mom"] = calculations.calc_dipole_moment(
            nu=lines[isotope]["nu"], einstein_a=lines[isotope]["a"]
        )
        lines[isotope].to_pickle(f"sim_data/{molecule.name}_{isotope}_lines.pkl")
    return lines


# Methods to extract quantum numbers from HITRAN data
def calc_resonances(df: DataFrame, detunings: bool = False) -> DataFrame:
    """Calculate resonance frequencies and corresponding detunings.

    :key 2p_res_: Resonance frequency for 2 photon resonance. (similar 3p_res)
    :key delta_1p_bei_2p_res: Detuning of one photon (or 2 or 3) for a laser
        tuned at 2 photon resonance.

    :param bool detunings: Add detunings to the dataframe
    :returns: data frame expanded with the resonance frequencies and detunings.
    """
    df["2p_res_"] = (df["e_pp_2_3"] - df["e_pp_0_1"]) / 2
    df["3p_res_"] = (df["e_pp_0_3"] + df["nu_0_3"] - df["e_pp_0_1"]) / 3
    if detunings:
        df["delta_1p_bei_2p_res"] = df["e_pp_1_2"] - (1 * df["2p_res_"] + df["e_pp_0_1"])
        df["delta_2p_bei_2p_res"] = df["e_pp_2_3"] - (2 * df["2p_res_"] + df["e_pp_0_1"])
        df["delta_3p_bei_2p_res"] = (
            df["e_pp_0_3"] + df["nu_0_3"] - (3 * df["2p_res_"] + df["e_pp_0_1"])
        )  # noqa: E501
        df["delta_1p_bei_3p_res"] = df["e_pp_1_2"] - (1 * df["3p_res_"] + df["e_pp_0_1"])
        df["delta_2p_bei_3p_res"] = df["e_pp_2_3"] - (2 * df["3p_res_"] + df["e_pp_0_1"])
        df["delta_3p_bei_3p_res"] = (
            df["e_pp_0_3"] + df["nu_0_3"] - (3 * df["3p_res_"] + df["e_pp_0_1"])
        )  # noqa: E501
    return df


def calc_transitions(
    df_0_1: DataFrame, df_1_2: DataFrame, df_2_3: DataFrame, df_0_3: DataFrame, parts: int = 3
) -> DataFrame:
    """Calaculate cartesian product of dataframe columns of 0->1, 1->2, 2->3 and 0->3
    transitions to get every possible combination of transitions

    :param dataframe df_lower_upper: Dataframes of transitions from lower to upper state.
    :returns: dataframe with all the transitions combined into different pathways.
    """

    # determine whether electronic states are defined in all necessary lists.
    el = "el" in df_2_3 and "el" in df_0_3

    # Calaculate the cartesian product of pandas df columns
    def cartesian_product(d):
        index = pd.MultiIndex.from_product(d.values(), names=d.keys())
        return pd.DataFrame(index=index).reset_index()

    # Calaculate the cartesian product for j_pp
    df_res_j_pp = cartesian_product(
        {
            "j_pp_0_1": df_0_1["j_pp"].astype(np.int32),
            "j_pp_1_2": df_1_2["j_pp"].astype(np.int32),
            "j_pp_2_3": df_2_3["j_pp"].astype(np.int32),
            "j_pp_0_3": df_0_3["j_pp"].astype(np.int32),
        }
    )

    # Calaculate the cartesian product for j_p
    df_res_j_p = cartesian_product(
        {
            "j_p_0_1": df_0_1["j_p"].astype(np.int32),
            "j_p_1_2": df_1_2["j_p"].astype(np.int32),
            "j_p_2_3": df_2_3["j_p"].astype(np.int32),
            "j_p_0_3": df_0_3["j_p"].astype(np.int32),
        }
    )

    if el:
        # Calaculate the cartesian product for j_pp
        df_res_el_2 = cartesian_product(
            {
                "j_pp_0_1": df_0_1["j_pp"].astype(np.int32),
                "j_pp_1_2": df_1_2["j_pp"].astype(np.int32),
                "el_2": df_2_3["el"].astype(np.bool_),
                "j_pp_0_3": df_0_3["j_pp"].astype(np.int32),
            }
        )

        # Calaculate the cartesian product for j_p
        df_res_el_0 = cartesian_product(
            {
                "j_p_0_1": df_0_1["j_p"].astype(np.int32),
                "j_p_1_2": df_1_2["j_p"].astype(np.int32),
                "j_p_2_3": df_2_3["j_p"].astype(np.int32),
                "el_0": df_0_3["el"].astype(np.bool_),
            }
        )

    """Filter for j_p_0_1 == j_pp_1_2, j_p_1_2 == j_pp_2_3 and j_p_2_3 == j_p_0_3
    and the electronic state, if applicable
    """

    df_transitions = None
    count = len(df_res_j_pp) // parts
    for i in range(parts):
        end = None if i == parts - 1 else (i + 1) * count
        j_pp = df_res_j_pp[i * count : end]
        j_p = df_res_j_p[i * count : end]
        if el:
            el_2 = df_res_el_2[i * count : end]
            el_0 = df_res_el_0[i * count : end]
            filtered = j_pp[
                (j_p["j_p_0_1"] == j_pp["j_pp_1_2"])
                & (j_p["j_p_1_2"] == j_pp["j_pp_2_3"])
                & (j_p["j_p_2_3"] == j_p["j_p_0_3"])
                & (el_2["el_2"] == el_0["el_0"])
            ]
        else:
            filtered = j_pp[
                (j_p["j_p_0_1"] == j_pp["j_pp_1_2"])
                & (j_p["j_p_1_2"] == j_pp["j_pp_2_3"])
                & (j_p["j_p_2_3"] == j_p["j_p_0_3"])
            ]
        if df_transitions is None:
            df_transitions = filtered
        else:
            df_transitions = pd.concat((df_transitions, filtered))

    # Merge filtered dataframe with unfiltered j_p dataframe
    df_transitions = pd.merge(
        df_transitions,  # type: ignore
        df_res_j_p,
        left_index=True,
        right_index=True,
    )

    if el:  # merge also electronic information
        df_transitions = pd.merge(
            df_transitions, df_res_el_2["el_2"], left_index=True, right_index=True
        )
        df_transitions = pd.merge(
            df_transitions, df_res_el_0["el_0"], left_index=True, right_index=True
        )
        # delete these dfs
        del df_res_el_2, df_res_el_0

    # Delete unnecessary data
    del df_res_j_pp, df_res_j_p
    gc.collect()

    for value in ("nu", "e_pp", "gamma_air", "gamma_self", "n_air", "delta_air", "a", "dipole_mom"):
        df_transitions = pd.merge(
            df_transitions,
            cartesian_product(
                {
                    f"{value}_0_1": df_0_1[value].astype(np.float32),
                    f"{value}_1_2": df_1_2[value].astype(np.float32),
                    f"{value}_2_3": df_2_3[value].astype(np.float32),
                    f"{value}_0_3": df_0_3[value].astype(np.float32),
                }
            ),
            left_index=True,
            right_index=True,
        )

    return df_transitions


def calculate_transition_paths(
    molecule: BaseMolecule, lines: dict[int, DataFrame]
) -> dict[int, DataFrame]:
    """Calculate transition paths and store them in a file per isotope."""

    # definition for electronic state

    def add_electronic_state(df: DataFrame, dipole_mom: float, e_p: float) -> DataFrame:
        """Add electronic states.

        :param dataframe df: data
        :param dipole_mom: dipole moment of electronic state in Cm
        :param e_p: Energy of upper level in cm^-1
        :returns: datframe concatenated of df and the electronic states.
        """
        # TODO science: are all rotational/vibrational states allowed or only a subset?
        el = df.copy(deep=True)
        el = el.loc[el["branch"] == "R"]
        el.loc[:, "dipole_mom"] = dipole_mom
        el.loc[:, "nu"] = e_p - el.loc[:, "e_pp"]  # Transition frequency in cm^-1 to excited state
        el.loc[:, "el"] = True
        df.loc[:, "el"] = False
        return pd.concat((df, el))

    # Filter the dataframes for the calculation of the nonlinear susceptibility

    filtered_lines: dict[int, dict[str, DataFrame]] = {}

    for isotope in molecule.isotopes:
        filtered_lines[isotope] = {}
        for lower, upper in ((0, 1), (1, 2), (2, 3), (0, 3)):
            filtered_lines[isotope][f"{lower}_{upper}"] = molecule.filter_lines(
                lines[isotope], lower, upper
            )

    # Compute electronic states (requires step before!)

    for isotope in molecule.isotopes:
        filtered_lines[isotope]["2_3"] = add_electronic_state(
            filtered_lines[isotope]["2_3"], *molecule.electronic_state
        )
        filtered_lines[isotope]["0_3"] = add_electronic_state(
            filtered_lines[isotope]["0_3"], *molecule.electronic_state
        )

    # Calculate transition paths (requires some time)

    paths: dict[int, DataFrame] = {}

    start = perf_counter()
    for isotope in molecule.isotopes:
        paths[isotope] = calc_transitions(
            filtered_lines[isotope]["0_1"],
            filtered_lines[isotope]["1_2"],
            filtered_lines[isotope]["2_3"],
            filtered_lines[isotope]["0_3"],
            parts=5,
        )
        calc_resonances(paths[isotope], detunings=False)  # add resonance information to the df.
        paths[isotope].to_pickle(f"sim_data/{molecule.name}_{isotope}_paths.pkl")
        log.info(f"Calculating transition paths finished in {perf_counter() - start} s.")
        start = perf_counter()

    return paths


if __name__ == "__main__":
    main()
