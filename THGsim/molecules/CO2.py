"""
All relevant information regarding CO2

"""

import logging
import re

import numpy as np
from pandas import DataFrame
from scipy import constants

from . import BaseMolecule

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class CO2(BaseMolecule):
    """Parameters"""
    name = "CO2"  # file names
    id = 2  # HITRAN molecule id
    isotopes = [1]  # list of HITRAN isotope IDs
    nu_range = (0, 10000)  # range in inverse cm to get transitions in.
    # Isotope details. Key is isotope id.
    formula = {  # from HITRAN
        1: "$^{12}$C$^{16}$O$_2$",
        2: "$^{13}$C$^{16}$O$_2$",
    }
    molar_mass = {  # kg / mol, from HITRAN
        1: 43.989830e-3,
        2: 44.993185e-3,
    }
    abundance = {  # relative, from HITRAN
        1: 0.984204,
        2: 0.0110574,
    }

	# Molecule details.
    # Source: https://doi.org/10.1039/D2CP01017H  Ultrafast CO2 photodissociation in the energy region of the lowest Rydberg series,
    # Johan F. Triana et al. Phys. Chem. Chem. Phys., 2022,24, 14072-14084
    electronic_state = 5.443e-30, 89044  # dipole moment in Cm and transition in cm^-1

    def _extractQuanta(self, table_name: str, column_name: str) -> list:
        """Extract the quantum numbers (except from local_lower_quanta)"""
        quanta_list = self.hapi.getColumn(table_name, column_name)
        data = []
        for quanta in quanta_list:
            list2 = [int(i) for i in (re.findall(r'\d', quanta))]
            data.append(list2)
        return data

    def _extractQuanta2(self, table_name: str, column_name: str) -> list:
        """Extract the quantum numbers from local_lower_quanta"""
        quanta_list = self.hapi.getColumn(table_name, column_name)
        data = []
        for quanta in quanta_list:
            if len((re.findall(r'[A-Z]|[0-999]|[a-z]', quanta))) == 3:
                list2 = [(i) for i in (re.findall(r'[A-Z]|[0-9]|[a-z]', quanta))]
            elif len((re.findall(r'[A-Z]|[0-999]|[a-z]', quanta))) == 4:
                list2 = (re.findall(r'[A-Z]|[0-9][0-9]|[a-z]', quanta))
            elif len((re.findall(r'[A-Z]|[0-999]|[a-z]', quanta))) == 5:
                list2 = (re.findall(r'[A-Z]|[0-9][0-9][0-9]|[a-z]', quanta))
            else:
                print(len((re.findall(r'[A-Z]|[0-999]|[a-z]', quanta))))
                continue
            data.append(list2)
        return data

    def interpret_transitions(self, hapi, table_name: str) -> DataFrame:
        """Take HITRAN data and extract relevant information."""
        self.hapi = hapi
        global_upper_quanta = self._extractQuanta(table_name, 'global_upper_quanta')
        global_lower_quanta = self._extractQuanta(table_name, 'global_lower_quanta')

        return DataFrame({
            "nu": self.makeList(hapi.getColumn(table_name, 'nu')),
            "v1_p": [item[0] for item in global_upper_quanta],
            "v2_p": [item[1] for item in global_upper_quanta],
            "l_p": [item[2] for item in global_upper_quanta],
            "v3_p": [item[3] for item in global_upper_quanta],
            "r_p": [item[4] for item in global_upper_quanta],
            "v1_pp": [item[0] for item in global_lower_quanta],
            "v2_pp": [item[1] for item in global_lower_quanta],
            "l_pp": [item[2] for item in global_lower_quanta],
            "v3_pp": [item[3] for item in global_lower_quanta],
            "r_pp": [item[4] for item in global_lower_quanta],
            "upp. rot. q.": self._extractQuanta(table_name, 'local_upper_quanta'),
            "branch": [(item[0]) for item in self._extractQuanta2(table_name,
                                                                  'local_lower_quanta')],
            "a": self.makeList(hapi.getColumn(table_name, 'a')),
            "j_pp": [int(item[1]) for item in self._extractQuanta2(table_name,
                                                                   'local_lower_quanta')],
            "low. rot. q.": self._extractQuanta2(table_name, 'local_lower_quanta'),
            "e_pp": self.makeList(hapi.getColumn(table_name, 'elower')),
            "n_air": self.makeList(hapi.getColumn(table_name, 'n_air')),
            "gamma_air": self.makeList(hapi.getColumn(table_name, 'gamma_air')),
            "gamma_self": self.makeList(hapi.getColumn(table_name, 'gamma_self')),
            "delta_air": self.makeList(hapi.getColumn(table_name, 'delta_air')),
            "g_pp": self.makeList(hapi.getColumn(table_name, 'gpp')),
            "g_p": self.makeList(hapi.getColumn(table_name, 'gp')),
        })

    @staticmethod
    def filter_lines(dataframe: DataFrame, lower_vibration: int, upper_vibration: int) -> DataFrame:
        """Return a filtered dataframe.

        Transitions in v3: 0->1, 1->2, 2->3 and 0->3 with v1_pp, v1_p, v2_pp, v2_p = 0

        :param df: dataframe
        :param lower_vibration: lower level vibration number
        :param uppwer_vibration: upper level vibration number
        """
        return dataframe[(dataframe['v1_pp'] == 0)
                  & (dataframe['v1_p'] == 0)
                  & (dataframe['v2_pp'] == 0)
                  & (dataframe['v2_p'] == 0)
                  & (dataframe['v3_pp'] == lower_vibration)
                  & (dataframe['v3_p'] == upper_vibration)
                  ]

    @staticmethod
    def _calc_refractive_index_offset_CO2(nu, p, t):
        # refrac_index_CO2_Offset_literature  = (154.489/ (0.0584738 - (1/THGconvert.nu_to_lambda(nu)) **2 ) + 8309192  / (210.92417 - (1/THGconvert.nu_to_lambda(nu)) **2 ) + 287641.90  / (60.122959 - (1/THGconvert.nu_to_lambda(nu)) **2 )) * 10 ** -8
        # refrac_index_CO2_Offset_literature  = (0.154489/ (0.0584738 - (1/((nu)/10000)) **2 ) + 8309.1927  / (210.92417 - (1/((nu)/10000) **2 )) + 287.64190  / (60.122959 - (1/((nu)/10000) **2 ))) * 10 ** -5
        # refrac_index_CO2_Offset_literature = (0.00000154489/ (0.0584738 - (1/THGconvert.nu_to_lambda(nu)/1000) **2 ) + 0.083091927  / (210.92417 - (1/THGconvert.nu_to_lambda(nu)/1000) **2 ) + 0.0028764190  / (60.122959 -(1/THGconvert.nu_to_lambda(nu)/1000) **2 ))
        # refrac_index_CO2_Offset = (refrac_index_CO2_Offset_literature * p / 94922.54) * (1 + p * (6.72112 - 0.0777879*(t- 273.15) + 0.0004250*(t- 273.15)**2) * 10**-8)/ (1 + 0.0036610*(t- 273.15))

        # INTERFEROMETRIC DETERMINATION OF THE REFRACTIVE INDEX OF CARBON DIOXIDE IN THE ULTRAVIOLET REGION  # noqa: E501
        refrac_index_CO2_Offset_literature = (1205.5 * (5.79925 / (166.175 - (nu / 10000)**2))
                                              + (0.12005 / (79.609 - (nu / 10000)**2))
                                              + (0.53334 * 10 ** -2 / (56.3064 - (nu / 10000)**2))
                                              + (0.43244 * 10 ** -2 / (46.0196 - (nu / 10000)**2))
                                              + (1.218145 * 10 ** -4 / (0.0584738 - (nu / 10000)**2))) * 10 ** -5  # noqa: E501

        # Precision refractive index measurements of air, 𝗡𝟮 , 𝗢𝟮 , Ar, and 𝗖𝗢𝟮 with a
        # frequency comb
        refrac_index_CO2_Offset = (
            (refrac_index_CO2_Offset_literature * p / 94922.54)
            * (
                1
                + p * (6.72112 - 0.0777879 * (t - 273.15) + 0.0004250 * (t - 273.15) ** 2) * 10**-8
            )  # noqa: E501
            / (1 + 0.0036610 * (t - 273.15))
        )
        return refrac_index_CO2_Offset

    def modify_ref_index_fund(self, ref_index_without_offset, nu_grid, pressure, temperature):
        """Calculate the refractive index."""
        return ref_index_without_offset + self._calc_refractive_index_offset_CO2(nu_grid, pressure,
                                                                                 temperature)

    def modify_ref_index_signal(self, ref_index_without_offset, nu_grid, pressure, temperature):
        """Modify the refractive index."""
        return ref_index_without_offset + self._calc_refractive_index_offset_CO2(nu_grid, pressure,
                                                                                 temperature)

    @staticmethod
    def vibrational_partition_function(temperature: float) -> float:
        """Calculate the vibrational partition function for a given temperature.

        :param temperature: in Kelvin.
        """
        beta = 1 / (constants.k * temperature)
        vibrational_partition_function_nu1 = (1 / (1 - np.exp(-beta * constants.h * constants.c * 1388 * 100)))  # noqa: E501
        vibrational_partition_function_nu2 = (1 / (1 - np.exp(-beta * constants.h * constants.c * 667.4 * 100)))  # noqa: E501
        vibrational_partition_function_nu3 = (1 / (1 - np.exp(-beta * constants.h * constants.c * 2349 * 100)))  # noqa: E501
        return (vibrational_partition_function_nu1 * vibrational_partition_function_nu2**2
                * vibrational_partition_function_nu3)


class Molecule(CO2):
    pass
