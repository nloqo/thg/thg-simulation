"""Definition of molecules."""

from typing import Optional, Protocol

from pandas import DataFrame


class BaseMolecule(Protocol):
    """Molecule definitions."""

    name: str  # file names
    id: int  # HITRAN molecule id
    isotopes: list[int]  # list of HITRAN isotope IDs
    nu_range: tuple[float, float]  # range in inverse cm to get transitions in.
    # Isotope details. Key is isotope id. Source is HITRAN.
    formula: Optional[dict[int, str]] = None
    molar_mass: dict[int, float]  # in kg / mol. Key is isotope ID
    abundance: dict[int, float]  # relative abundance. Key is isotope ID

	# Molecule details. Source TBD.
    electronic_state: tuple[float, float]  # dipole moment in Cm and transition in cm^-1

    @staticmethod
    def makeList(col: list[str] | tuple[str]) -> list[float]:
        """Make a list from a HITRAN data column"""
        data = []
        for elm in col:
            data.append(float(elm))
        return data

    def interpret_transitions(self, hapi, table_name: str) -> DataFrame:
        """Take HITRAN data and extract relevant information."""
        ...

    @staticmethod
    def filter_lines(dataframe: DataFrame, lower_vibration: int, upper_vibration: int) -> DataFrame:
        """Return a filtered dataframe, with transitions satisfying from lower to upper vibration.

        :param dataframe: dataframe
        :param lower_vibration: lower level vibration number
        :param uppwer_vibration: upper level vibration number
        """
        ...

    def filter_lines_for_occupation(self, dataframe: DataFrame) -> DataFrame:
        """Filter the lines to get all states for occupation calculation."""
        return self.filter_lines(dataframe=dataframe, lower_vibration=0, upper_vibration=1)

    @staticmethod
    def filter_lines_for_refractive_index(dataframe: DataFrame) -> DataFrame:
        """Filter the lines for the refractive index."""
        # for example for vibrational transitions: df[(df["v1_pp"] == 0) & (df["v1_p"] >= 1)]
        return dataframe

    def modify_ref_index_fund(
        self, ref_index_without_offset, nu_grid, pressure: float, temperature: float
    ):
        """Modify the refractive index to incorporate electronic transitions."""
        ...

    def modify_ref_index_signal(
        self, ref_index_without_offset, nu_grid, pressure: float, temperature: float
    ):
        """Modify the refractive index to incorporate electronic transitions."""
        ...

    @staticmethod
    def vibrational_partition_function(temperature: float) -> float:
        """Calculate the vibrational partition function for a given temperature.

        :param temperature: in Kelvin.
        """
        ...
