"""
All relevant information regarding HCl

"""

import logging
import re

import numpy as np
from pandas import DataFrame
from scipy import constants

from . import BaseMolecule


log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class HCl(BaseMolecule):
    """Parameters"""
    name = "HCl"  # file names
    id = 15  # HITRAN molecule id
    isotopes = [1, 2]  # list of HITRAN isotope IDs
    nu_range = (0, 1e6)  # range to get transitions in.
    # Isotope details. Key is isotope id. Source is HITRAN.
    formula = {
        1: "H$^{35}$Cl",
        2: "H$^{37}$Cl",
    }
    molar_mass = {  # kg / mol
        1: 35.976678e-3,
        2: 37.973729e-3,
    }
    abundance = {  # relative
        1: 0.757587,
        2: 0.242257
    }

	# Molecule details. Source "Quantum Chemical Study of the Potential Energy Curves and Electronic Transition Strengths in HCl, XeCl, and HCl + Xe", Adams, George F. and Chabalowski, Cary F., 10.1021/j100074a011
    electronic_state = 3.14e-30, 63718  # dipole moment in Cm and transition in cm^-1

    @staticmethod
    def _extractQuanta2_HCl(quanta_list):
        """Extract the quantum numbers from local_lower_quanta for HCl"""
        list = []
        for quanta in quanta_list:
            list2 = [(i) for i in (re.findall(r'[A-Z]|[0-999]\.*[0-999]*', quanta))]
            list.append(list2)
        return list

    def interpret_transitions(self, hapi, table_name) -> DataFrame:
        """Take HITRAN data and extract relevant information."""
        makeList = self.makeList
        return DataFrame({
            "nu": makeList(hapi.getColumn(table_name, 'nu')),
            "a": makeList(hapi.getColumn(table_name, 'a')),
            "gamma_air": makeList(hapi.getColumn(table_name, 'gamma_air')),
            "gamma_self": makeList(hapi.getColumn(table_name, 'gamma_self')),
            "e_pp": makeList(hapi.getColumn(table_name, 'elower')),
            "n_air": makeList(hapi.getColumn(table_name, 'n_air')),
            "delta_air": makeList(hapi.getColumn(table_name, 'delta_air')),
            "v1_p": makeList(hapi.getColumn(table_name, 'global_upper_quanta')),
            "v1_pp": makeList(hapi.getColumn(table_name, 'global_lower_quanta')),
            "local_upper_quanta": (hapi.getColumn(table_name, 'local_upper_quanta')),
            "local_lower_quanta": (hapi.getColumn(table_name, 'local_lower_quanta')),
            "branch": [(item[0]) for item in self._extractQuanta2_HCl(
                hapi.getColumn(table_name, 'local_lower_quanta'))],
            "j_pp": [int(item[1]) for item in self._extractQuanta2_HCl(
                hapi.getColumn(table_name, 'local_lower_quanta'))],
            "g_pp": makeList(hapi.getColumn(table_name, 'gpp')),
            "g_p": makeList(hapi.getColumn(table_name, 'gp')),
        })

    @staticmethod
    def filter_lines(dataframe: DataFrame, lower_vibration: int, upper_vibration: int) -> DataFrame:
        """Return a filtered dataframe.

        Transitions in v1: 0->1, 1->2, 2->3 and 0->3

        :param df: dataframe
        :param lower_vibration: lower level vibration number
        :param uppwer_vibration: upper level vibration number
        """
        return dataframe[
            (dataframe["v1_pp"] == lower_vibration) & (dataframe["v1_p"] == upper_vibration)
        ]

    @staticmethod
    def _scale_ref_index_offset(offset_0: float, pressure: float, temperature: float) -> float:
        return (pressure * offset_0) / (101300 * (1 + (temperature - 273.15) * 3.66e-3))

    # Refractive index fundamental
    # source: Chamberlain (1965) https://doi.org/10.1364/AO.4.001382
    # parameters: 760torr, 0°C
    # one typo in table I corrected: 2760cm^-1: 455.9 instead of 465.9 (in accordance with fig. 4).
    ref_index_fund_lit = np.array([
        [
            2500, 2520, 2540, 2560, 2580, 2600, 2620, 2640, 2660, 2680,
            2700, 2720, 2740, 2760, 2780, 2800, 2820, 2840, 2860, 2880, 2900,
            2920, 2940, 2960, 2980, 3000, 3020, 3040, 3060, 3080, 3100, 3120,
            3140, 3160, 3180, 3200, 3220, 3240, 3260, 3280, 3300,
        ],
        [
            444.5, 444.8, 445.5, 445.8, 446.8, 447.5, 448.3, 449.9,
            451.1, 452.4, 453.7, 455.6, 457.0, 455.9,
            454.4, 451.1, 448.3, 444.3, 440.5, 443.5, 455.1, 453.2,
            446.6, 438.8, 430.6, 426.8, 426.9, 429.6, 431.8, 434.8,
            436.6, 437.5, 438.1, 437.9, 438.7, 439., 439.3, 439.4,
            439.6, 440.1, 440.5,
        ]
    ])

    """
    Frequencies in the middle of two  1P vibrational resonances, including first and last value of
    the literature values.

    find resonances between literature values
    `resonances = lines[1][(lines[1]["v1_pp"] == 0) & (lines[1]["v1_p"] == 1)]["nu"]`
    `nu_r = resonances[(2500<resonances) & (resonances < 3300)]`
    calculate the mean value: `nu_ir = (nu_r[1:] + nu_r[:-1]) / 2`
    """
    nu_inter_resonances = np.array([2500,
        2530.2746785,       2558.0742535,        2585.445122,
       2612.3736725,          2638.8463,       2664.8495025,
       2690.3697375,       2715.3934965,        2739.907319,
       2763.8978055,        2787.351633, 2810.2555684999998,
 2832.5964814999998, 2854.3613564999996,       2885.6724485,
       2916.0716155,        2935.404955,        2954.099362,
       2972.1427755, 2989.5233319999998,        3006.229378,
 3022.2494770000003, 3037.5724179999997, 3052.1872249999997,
       3066.0831635,        3079.249687,        3091.676602,
        3103.353991, 3110.3897150000003,        3115.657752,
 3121.1438399999997,        3126.025188, 3131.0447750000003,
        3135.534913, 3140.0909435000003,       3144.1854875,
       3148.2808145, 3151.9755130000003, 3155.6128879999997,
        3158.903626,       3162.0858875,       3164.9686815,
       3167.6985035,       3170.1695005,       3172.4496185,
        3174.505094,       3176.3382615,        3177.974615,
        3179.363622,        3180.577373,        3181.525059,
        3182.312841, 3182.8221080000003, 3183.1806644999997, 3300
    ])
    # calculated for no lines filtering
    ref_index_fund_offset = np.array([
        457.44676304-0.00613033j, 457.26068391-0.00673874j,
        456.89693316-0.00758141j, 457.1666385 -0.00890238j,
        456.82909472-0.01120745j, 457.26947186-0.0156406j ,
        457.00288995-0.02518247j, 456.30971188-0.04618208j,
        455.66656378-0.09317196j, 454.80316366-0.18922698j,
        451.60960484-0.35752916j, 449.57300596-0.59728131j,
        449.53348224-0.83960768j, 452.67708841-0.98104849j,
        456.14162055-1.01593368j, 472.88058574-0.21681049j,
        463.0468322 -0.41144936j, 458.08549959-0.69123586j,
        456.98958487-0.83259489j, 455.67117228-0.77332389j,
        454.77757612-0.57887015j, 454.79975762-0.37070807j,
        455.06558732-0.21161756j, 455.64495994-0.11380713j,
        455.42015676-0.06261092j, 455.48274417-0.03638301j,
        456.1385044 -0.0231805j , 456.41424315-0.01670359j,
        456.35253189-0.0133292j , 456.27159852-0.01281651j,
        456.17864393-0.01119664j, 456.12798605-0.01057033j,
        456.09310624-0.00992231j, 456.05130934-0.00941307j,
        455.97444725-0.00903049j, 455.83565047-0.00862424j,
        455.64600504-0.00836197j, 455.42183633-0.00802979j,
        455.2193143 -0.00778927j, 455.04574857-0.00756594j,
        454.93153694-0.007375j  , 454.87615151-0.00719936j,
        454.87062545-0.00704732j, 454.89401022-0.00690916j,
        454.93095182-0.00678867j, 454.97192039-0.00668115j,
        455.00971225-0.00658709j, 455.04062546-0.0065054j ,
        455.06351582-0.00643416j, 455.07785008-0.00637491j,
        455.08550887-0.00632401j, 455.08812385-0.00628482j,
        455.08819571-0.00625262j, 455.08728549-0.00623198j,
        455.08621443-0.00621752j, 454.33864342-0.00354463j
        ])

    def _calc_refractive_index_offset_MIR(self, nu, p: float, temperature: float) -> list:

        from scipy.interpolate import interp1d

        try:
            refrac_index_HCl_Offset_MIR_fit = interp1d(
                self.nu_inter_resonances, self.ref_index_fund_offset, kind="cubic"
            )
        except NameError:
            raise ValueError("You did not calibrate the offsets!")

        def calc(nu: float) -> float:
            return (
                p
                * 1e-6
                * (refrac_index_HCl_Offset_MIR_fit(nu))
                / (101300 * (1 + (temperature - 273.15) * 3.66e-3))
            )
        vcalc = np.vectorize(calc)
        return vcalc(nu)

    def modify_ref_index_fund(
        self, ref_index_without_offset_fund_HCl, nu_grid_fund, pressure, temperature: float
    ):
        """Calculate the refractive index."""
        ref_index_offset_fund_HCl = self._calc_refractive_index_offset_MIR(
            nu_grid_fund, pressure, temperature
        )

        return ref_index_without_offset_fund_HCl + ref_index_offset_fund_HCl

    # Refractive index signal
    # Source for refractive index in near infrared:
    # Rollefson&Rollefson 1935 https://doi.org/10.1103/PhysRev.48.779
    # Data source is not very dense, therefore interpolation around single points for
    # different wavelengths
    ref_index_signal_lit = np.array([
        # wavenumber of point, offset_0 of refractive index / 1e6
        (1e7 / 1178, 442.4),  # rough approximation from figure at 1178 nm (THG signal)
        (1e7 / 664, 447.8),  # rough approximation from figure at 664 nm (SFM signal)
        (1e7 / 1064, 442.6),  # value from table  I  at  1064 nm
        (1e7 / 410, 455),  # guess work
        ]).T

    # calculated for no lines filtering
    ref_index_signal_offset = np.array(
        [
            444.09314822 - 8.71610390e-05j,
            448.33328023 - 1.49351728e-05j,
            443.97731195 - 6.23339311e-05j,
            455.20290873 - 3.49975356e-06j,
        ]
    )

    def modify_ref_index_signal(self, ref_index_without_offset, nu_grid, pressure, temperature):
        """Modify the refractive index."""
        m = np.nanmean(nu_grid)
        offset_0 = None
        for nu, offset_0 in zip(self.ref_index_signal_lit[0], self.ref_index_signal_offset):
            if np.abs(nu-m) < 100:
                break
        if offset_0 is None:
            raise ValueError("no value for your wavenumber")
        offset = self._scale_ref_index_offset(
            offset_0=offset_0*1e-6, pressure=pressure, temperature=temperature
        )
        return ref_index_without_offset + offset

    @staticmethod
    def vibrational_partition_function(temperature: float) -> float:
        """Calculate the vibrational partition function for a given temperature.

        :param temperature: in Kelvin.
        """
        beta = 1 / (constants.k * temperature)
        return (1 / (1 - np.exp(-beta * constants.h * constants.c * 2883 * 100)))

    def calibrate_molecule(
        self, lines: dict[int, DataFrame], calc_linear_susceptibility_full_molecule
    ) -> None:
        """Calibrate the offset such that the simulation + offset represents literature data.

        Lines has to be calculated for 0°C=273.15 K.
        """
        temperature = 273.15  # K
        pressure = 101325  # Pa

        # fund
        fund_offset = (np.sqrt(1 + calc_linear_susceptibility_full_molecule(
            self.nu_inter_resonances,
            lines=lines,
            molecule=self,
            temperature=temperature,
            pressure_self=pressure,
            pressure_air=0,
        )) - 1) * 1e6

        from scipy.interpolate import interp1d

        literature_function = interp1d(
            self.ref_index_fund_lit[0], self.ref_index_fund_lit[1], kind="cubic"
        )

        self.ref_index_fund_offset = literature_function(self.nu_inter_resonances) - fund_offset

        # signal
        signal_offset = (np.sqrt(1 + calc_linear_susceptibility_full_molecule(
            self.ref_index_signal_lit[0],
            lines=lines,
            molecule=self,
            temperature=temperature,
            pressure_self=pressure,
            pressure_air=0,
        )) - 1) * 1e6
        self.ref_index_signal_offset = self.ref_index_signal_lit[1] - signal_offset

class Molecule(HCl):
    pass
