
import pytest

from THGsim.calculations import (
    calc_natural_linewidth,
    calc_pressure_broad,
    calc_doppler_broad,
    calc_total_broadening_approx,
)

class HClLine:
    """Contain information about a specific HCl line.
    H35Cl, # v1_p = 1, j_p = 0, v1_pp = 0, j_pp = 1, nu = 2865.098165
    """
    nu = 2865.098165
    molar_mass = 0.035976678
    n_air = 0.65
    gamma_air = 0.0892
    gamma_self = 0.222


def test_calc_natural_linewidth():
    assert calc_natural_linewidth(1.5e8) == pytest.approx(0.00039816280941571086)


def test_calc_doppler_broad():
    # values taken from H35Cl, # v1_p = 1, j_p = 0, v1_pp = 0, j_pp = 1, nu = 2865.098165
    # molar mass from HITRAN for H35Cl
    # result from mathematica
    assert calc_doppler_broad(
        nu=HClLine.nu, temperature=273.15 + 25, molar_mass=HClLine.molar_mass
    ) == pytest.approx(0.00295373)


def test_calc_doppler_broad_lit():
    """
    [5] J. Tennyson et al., "Recommended isolated-line profile for representing
    high-resolution spectroscopic transitions (IUPAC Technical Report)",
    Pure Appl. Chem. 86, 1931-1943 (2014). https://doi.org/10.1515/pac-2014-0208 [ADS]

    It gives the factor per gram per mol, and per K for any wavelength.
    """
    assert calc_doppler_broad(1, 1, 1e-3) == pytest.approx(3.581163e-7)


def test_calc_pressure_broad():
    # values taken from H35Cl, # v1_p = 1, j_p = 0, v1_pp = 0, j_pp = 1, nu = 2865.098165
    assert calc_pressure_broad(
        temperature=273.15 + 25,
        p_self=1000 * 100,
        n_air=HClLine.n_air,
        gamma_air=HClLine.gamma_air,
        gamma_self=HClLine.gamma_self,
        p_air=0,
    ) == pytest.approx(0.218123, rel=1e-3)
    # result from mathematica


def test_calc_total_linewidth_FWHM():
    ## result from mathematica
    temperature=273.15 + 25  # K
    pressure = 1000 * 100  # Pa
    assert 2 * calc_total_broadening_approx(
        gamma_doppler_broadened=calc_doppler_broad(HClLine.nu, temperature, HClLine.molar_mass),
        gamma_pressure_broadened=calc_pressure_broad(
            temperature,
            HClLine.n_air,
            HClLine.gamma_air,
            p_air=0,
            p_self=pressure,
            gamma_self=HClLine.gamma_self,
        ),
    ) == pytest.approx(0.436332, rel=1e-3)


