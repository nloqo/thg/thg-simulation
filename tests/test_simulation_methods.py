import numpy as np
from numpy.testing import assert_allclose

from THGsim.simulation_methods import calc_delta_k, calc_delta_k_fwm


def test_delta_k():
    nu_fund = 1e7 / np.array((3530, 3533, 3535))
    n_fund = np.array((1.1, 1.1, 1.1))
    n_sig = 1.15
    actual = calc_delta_k(nu_fund, n_fund, n_sig)
    assert_allclose(
        actual, np.array([266990.877076, 266764.16532, 266613.237928])
    )


def test_delta_k_vectorized():
    nu_fund = 1e7 / np.array((3530, 3533, 3535))
    n_fund = (1.1, 1.1, 1.1)
    n_sig = 1.15
    cdk = np.vectorize(calc_delta_k)
    actual = cdk(nu_fund, n_fund, n_sig)
    assert_allclose(
        actual, np.array([266990.877076, 266764.16532, 266613.237928])
    )


def test_delta_k_fwm():
    nu_probe = 1e7 / 1064
    n_probe = 1.1
    nu_fund = 1e7 / np.array((3530, 3533, 3535))
    n_fund = np.array((1.1,1.1,1.1))
    n_sig = 1.15
    cdk = np.vectorize(calc_delta_k_fwm)
    actual = cdk(nu_fund, nu_fund, nu_probe, n_fund, n_fund, n_probe, n_sig)
    assert_allclose(
        actual,
        np.array([473256.385493, 473105.244323, 473004.626062])
    )


def test_delta_k_fwm_start_single_probe():
    nu_probe = 1e7 / 1064
    n_probe = 1.1
    nu_fund = 1e7 / np.array((3530, 3533, 3535))
    n_fund = np.array((1.1,1.1,1.1))
    n_sig = 1.15
    vcalc = np.vectorize(calc_delta_k_fwm)
    actual = vcalc(nu_fund, nu_fund, nu_probe, n_fund, n_fund, n_probe, n_sig)
    assert_allclose(
        actual,
        np.array([473256.385493, 473105.244323, 473004.626062])
    )


def test_compare_delta_ks():
    nu_fund = 1e7 / np.array((3530, 3533, 3535))
    n_fund = (1.1,1.1,1.1)
    n_sig = 1.15
    dk = np.vectorize(calc_delta_k)
    dkf = np.vectorize(calc_delta_k_fwm)
    assert_allclose(
        dk(nu_fund, n_fund, n_sig),
        dkf(nu_fund, nu_fund, nu_fund, n_fund, n_fund, n_fund, n_sig),
    )

