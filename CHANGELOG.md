# CHANGELOG

## 0.2.0 - 2024-10-14

_The simulation is restructured in several files and refractive index calculation is improved._

### Changed
- Change values of electronic states of HCl and CO2 to match literature values (including source)
- Improve typing
- Expand documentation
- Remove function definitions from `simulation.py` into several files for easier access: `simulation_methods.py` and executable script `calculate_transition_paths.py`
- Expand the Readme for better usage
- Rename `Molecule` class to `BaseMolecule`, such that an actual molecule class (e.g. HCl) might be imported as `Molecule`
- Change calculation of HCl refractive index to match literature values instead of adding literature values to calculated values
- Restructure private HCl methods for refractive index calculation

### Added
- Add parameter `formula` to molecule classes with chemical formula
- Add the filter for calculation of state population to the molecule, instead of one filter for all
- Add a filter for the calculation of the refractive index (defaults to all lines) to the molecule class
- Add tests for some calculations

### Removed
- `QPM OPA1.py` calculations

### Fixed
- Fix Calculation of refractive index uses only vibrational transitions
- Fix molecules to use all transitions (not only irrelevant ones for CO2) for refractive index calculation
- Fix HCl refractive index calculation to reach literature values
- Fix Calculation of wave vector mismatch for SFM uses correct refractive indices


## 0.2.0b1 - 2024-03-14 initial version

_Made for CO2_

### Removed

- SNLOhelper extracted into its own repository and pypi package `snlo-helper`.


## 0.1.0 - 2023-01-06 HCl SFM paper version

_Initial version, made for the SFM paper_

_Detection of HCl molecules by resonantly enhanced sum-frequency mixing of mid- and near-infrared laser pulses_, Benedikt Moneke, Jan Frederic Kinder, Oskar Ernst, and Thomas Halfmann, Phys. Rev. A 107, 012803 – Published 5 January 2023, DOI [10.1103/PhysRevA.107.012803](https://doi.org/10.1103/physreva.107.012803).
